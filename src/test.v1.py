import symboling.hpp

symboling.StringType main_what("{name: what minLength: 6 checkPattern: '[-A-Za-z_0-9]+' atomic: false}")
symboling.StringType  main_what1("{name: what1 atomic: false}")
symboling.StringType  main_when("{name: when}");
symboling.NumericType main_how_many("{name: how-many min: 0 eps: 1 max: 100")
symboling.RecordType main_record("{name: record required: [about, when] types: { about: what } forbidden: [where] weight: {when: 2}}")
symboling.StringType main_words("{name: words atomic: false}")
symboling.ListType main_sentence("{name: sentence itemType: words deleteCost: 4}")
symboling.RecordType main_dateStandardFormat("{ name: date parseInput: '([0-9]*)-([0-9]*)-([0-9]*)' parseOutput: '{year: $1 month: $2 day: $3}'}")
symboling.EnumType main_keywords("{name: digits itemType: string values: [ one two three] }")
symboling.StringType main_word("{name: word }")
symboling.SetType main_words("{itemType: word  deleteCost: 1 editCost: 10}")
symboling.NumericType main_number("{name: number min: -1 max: 1 precision: 0.05}")
symboling.SetType main_numbers("{itemType: number}")

def main():
    # User defined types, checks and distances for records
        symboling.Type.addType(symboling.StringType("{name: what2 atomic: false}"))
        data0 = symboling.Value("{about: 'Hi world!' where: nowhere how-many: -12}", True)
        data1 = symboling.Value("{when: 1988-11-25 about: 'Hi-world' how-many: 13 what1: none}", True)
        data2 = symboling.Value("{about: 'Hello_world' when: 1992-12-16 how-many: 11 what2: none}", True)
        data0.check(), data1.check(), data2.check()
        # Checks that checking is done as expected
        check0 = main_record.getCheckMessage(data0)
        check1 = main_record.getCheckMessage(data1)
        symboling.alert(not(symboling.regexMatch(check0, "(\n|.)*required field 'when' missing(\n|.)*") and symboling.regexMatch(check0, "(\n|.)*forbidden field 'where' present(\n|.)*") and symboling.regexMatch(check0, "(\n|.)*name: what .* incorrect syntax(\n|.)*") and symboling.regexMatch(check0, "(\n|.)*name: how-many .* out of bound(\n|.)*") and check1 == ""), " illegal-state", "in symboling::test/check[01] bad check0=«%s» or check1=«%s»", check0, check1)
        # Calculates the distance and path between the elements
        symboling.alert(main_record.getDistance(data1, data2) != 25 or main_record.getPath(data1, data2).length() != 16, " illegal-state", "in symboling::test/record bad estimation:\n%s\n// Bad path %d:\n\t%s\n", main_record.asString().c_str(), main_record.getPath(data1, data2).length(), symboling.regexReplace(main_record.getPath(data1, data2).asString(), "\\{", "\n{").c_str())
    # Checks the recursive path generation on lists
        data1 = symboling.Value("[ Hello World Outsome ]", True)
        data2 = symboling.Value("[ Hi Nice World ]", True)
        main_sentence.getDistance(data1, data2)
        main_sentence.getPath(data1, data2).check()
        symboling.alert(main_sentence.getDistance(data1, data2) != 12 or main_sentence.getPath(data1, data2).length() != 16, " illegal-state", "in symboling::test/sentence bad estimation:\n%s\n// Bad path %d:\n\t%s\n", main_sentence.asString().c_str(), main_sentence.getPath(data1, data2).length(), symboling.regexReplace(main_sentence.getPath(data1, data2).asString(), "\\[", "\n[").c_str())
    # Editing distance depending on the cost balance
        lhs = symboling.Value("[-1, -0.9, -0.8, -0.7]", True)
        rhs = symboling.Value("[0.7, 0.8, 0.9, 1]", True)
        type1 = symboling.ListType("{name: always_delete_and_insert itemType: modal deleteCost: 1 editCost: 10}")
        symboling.alert(type1.getDistance(lhs, rhs) != 8 or type1.getPath(lhs, rhs).length() != 9, " illegal-state", "in symboling::test/type1 bad estimation:\n%s\n// Bad path %d:\n\t%s\n", type1.asString().c_str(), type1.getPath(lhs, rhs).length(), type1.getPath(lhs, rhs).asString().c_str())
        type1.getPath(lhs, rhs).check()
        type2 = symboling.ListType("{name: always_edit itemType: modal deleteCost: 10}")
        symboling.alert(abs(type2.getDistance(lhs, rhs) - 3.4) > 1e-6 or type2.getPath(lhs, rhs).length() != 5, " illegal-state", "in symboling::test/type2 bad estimation:\n%s\n// Bad path %d:\n\t%s\n", type2.asString().c_str(), type2.getPath(lhs, rhs).length(), type2.getPath(lhs, rhs).asString().c_str())
        type2.getPath(lhs, rhs).check()
    # Checks the regex parsing mechanism in record type
        result = main_dateStandardFormat.getValue("2022-02-30").asString()
        result0 = "{ year: 2022 month: 02 day: 30 }"
        symboling.alert(result != result0, " illegal-state", "in symboling::test/regex-parse unexpected result '" + result + "' != '" + result0 + "'")
    # Checks the distance and path on enumeration
        data0 = symboling.Value("one", True)
        data1 = symboling.Value("two", True)
        data2 = symboling.Value("too", True)
        symboling.alert(main_keywords.getValue(data1).asString() != main_keywords.getValue(data2).asString() or main_keywords.getDistance(data0, data1) != 3, " illegal-state", "in symboling::test/keywords bad estimation:\n%s\n", main_keywords.asString().c_str())
        main_keywords.getPath(data0, data1).check()
    # Checks the distance and path on set
        data1 = symboling.Value("[ Hello World Outsome Indeed ]", True)
        data1_sorted = symboling.Value("[ Hello Indeed Outsome World ]", True)
        data2 = symboling.Value("[ Nice World Hello ]", True)
        data3 = symboling.Value("[ Hello Nice Guy ]", True)
        symboling.alert(main_words.getValue(data1) is not data1_sorted, " illegal-state", "in symboling::test/words/sort bad normalisation '" + main_words.getValue(data1).asString() + "' != '" + data1_sorted.asString() + "'")
        symboling.alert(main_words.getDistance(data1, data2) != 3 or main_words.getPath(data1, data2).length() != 4, " illegal-state", "in symboling::test/words bad estimation:\n%s\n// Bad path %d:\n\t%s\n", main_words.asString().c_str(), main_words.getPath(data1, data2).length(), symboling.regexReplace(main_words.getPath(data1, data2).asString(), "\\[", "\n[").c_str())
        symboling.alert(main_words.getDistance(data1, data3) != 5 or main_words.getPath(data1, data3).length() != 6, " illegal-state", "in symboling::test/words bad estimation:\n%s\n// Bad path %d:\n\t%s\n", main_words.asString().c_str(), main_words.getPath(data1, data3).length(), symboling.regexReplace(main_words.getPath(data1, data3).asString(), "\\[", "\n[").c_str())
        data4 = symboling.Value("[ 0.3 0.5 0.3 0.1 0.102 ]", True)
        data4_normalized = main_numbers.getValue(data4)
        symboling.alert(not(abs(data4_normalized.get(0, 0.0) - 0.1) < 0.001 and abs(data4_normalized.get(1, 0.0) - 0.3) < 0.001 and abs(data4_normalized.get(2, 0.0) - 0.5) < 0.001), " illegal-state", "in symboling::test/words/sort bad normalisation '" + main_numbers.getValue(data4).asString() + "' != '" + data4_normalized.asString() + "'")
    # Mixed editing distance with user defined cost
        lhs = symboling.Value("[1, true, unknown, 0.5]", True)
        rhs = symboling.Value("[1, false, unknown]", True)
        type0 = Modalities()
        path = "[ [ 1 true unknown 0.5 ] [ 1 false unknown 0.5 ] [ 1 false unknown ] ]"
        symboling.alert(type0.getDistance(lhs, rhs) != 2 or type0.getPath(lhs, rhs).asString() != path, " illegal-state", "in symboling::test/type0 bad estimation:\n%s\n// Bad path %d:\n\t%s\n", type0.asString().c_str(), type0.getPath(lhs, rhs).length(), type0.getPath(lhs, rhs).asString().c_str())
    # Testing the NumericType normalization
        howmuch = symboling.NumericType("{ name: howmuch min: 0 max: 8 zero: 7}")
        symboling.alert((howmuch.getNormalized(2) - 0.520878) > 1e-6, " illegal-state", "in symboling::test/getNormalized bad estimation: getNormalized(2) = %f != 0", howmuch.getNormalized(2))

class Modalities(symboling.ListType):
    def __init__(self):
        super().__init__("{itemType: modal}")
    def cost(self, what, lhs, i, rhs, j):
        return 1

if __name__ == "__main__":
    main()


