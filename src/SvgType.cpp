#include "symboling.hpp"

namespace symboling {
  // Implements, as symboling types, a segment of the SVG https://www.w3.org/TR/SVG2 specification:
  void setSvgType()
  {
    static NumericType svg_numeric("{name: svg_numeric strict step: 1}");

    static RecordType svg_color("{name: svg_color strict types: {r: svg_numeric g: svg_numeric b: svg_numeric a: svg_numeric}}");

    static RecordType svg_style("{name: svg_style strict types: {fill: svg_color stroke: svg_color}}");

    static ListType svg_transformMatrix("{name: svg_transformMatrix strict itemType: svg_numeric minLength: 6 maxLength: 6}");

    static EnumType svg_segmentType("{name: svg_segmentType strict itemType: string values: [M L]}");

    static ListType svg_coordinatesList("{name: svg_coordinatesList strict itemType: svg_numeric}");

    static RecordType svg_svgSegment("{name: svg_svgSegment strict required: [type segment] types: {type: svg_segmentType segment: svg_coordinatesList}}");

    static ListType svg_svgD("{name: svg_svgD strict itemType: svg_svgSegment}");

    static RecordType svg_attributes("{name: svg_attributes strict types: {cx: svg_numeric cy: svg_numeric r: svg_numeric transform: svg_transformMatrix style: svg_style d: svg_svgD rx: svg_numeric ry: svg_numeric}}");

    static EnumType svg_svgTag("{name: svg_svgTag strict itemType: string values: [circle path ellipse]}");

    static RecordType svg_organ("{name: svg_organ strict required: [tag attr] strict: false types: {id: string tag: svg_svgTag attr: svg_attributes}}");

    static SetType svg_face("{name: svg_face strict itemType: svg_organ}");
  }
}
