@aideAPI

This is a deprecated preliminary implementation, use [symbolingtype](https://line.gitlabpages.inria.fr/aide-group/symbolingtype), [symbolingfield](https://line.gitlabpages.inria.fr/aide-group/symbolingfield), and [symbolingembedding](https://line.gitlabpages.inria.fr/aide-group/symbolingembedding), instead.

@slides ./symboling.pdf



