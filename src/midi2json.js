#!/usr/bin/env node

/**
 * @function midi2Jon
 * @static
 * @description Converts a file from MIDI to JSON format
 * ```
 *     Usage: ./src/midi2json.js input_file > output
 * ```
 * - Installation requires: `npm install [@tonejs/midi](https://www.npmjs.com/package/@tonejs/midi)`.
 */

const {
  Midi
} = require("@tonejs/midi");
const fs = require("fs");

function convertMidiToJSON(input) {
  try {
    const midiData = fs.readFileSync(input);
    const midi = new Midi(midiData);
    const data = JSON.stringify(midi);
    return data;
  } catch (error) {
    console.error(error);
    process.exit(-1);
  }
}

if (process.argv.length == 3 && fs.existsSync(process.argv[2])) {
  console.log(convertMidiToJSON(process.argv[2]));
} else {
  console.log("Usage " + process.argv[1] + " input_file > output" + (!fs.existsSync(process.argv[2]) ? " file '" + process.argv[2] + "' not found" : ""));
  process.exit(-1);
}
