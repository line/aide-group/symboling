Numerical quantity related to a sensory input or an algorithm numerical value, corresponds to a bounded value (between minimal and maximal bounds), up to a given precision threshold (above which two values may differs and below which two values are indistinguishable, being equal or not), an approximate neighborhood sampling size or ``step'' (below which two distinct values are in the same local area), a default value (used in initialization, or to avoid undefined value), expressed in a given unit (e.g., second, meter, etc), if any, as schematized in Fig.~\ref{measure-parameterization}.

\begin{figure}[htb]
 \centerline{\includegraphics[width=0.85\textwidth]{./measure-parameterization.png}}
 \caption{Specification of a numerical value. Metadata includes a name, a unit, a default value, bounds (minimum and maximum), a precision under which two values are not distinguishable, and a step value corresponding to a neighborhood size used to cover the value range.}
 \label{measure-parameterization}
\end{figure}

Such specification is important to properly manipulate quantitative information. In particular, the values can be normalized (e.g., mapped onto the $[-1, 1]$ interval) and mapped onto a finite set of relevant values. One consequence is that algorithm precision thresholds can be deduced (often using first-order approximations), spurious values can be detected, numerical conditioning of algorithms is enhanced, and so on (see \cite{vieville_implementing_2001} for a discussion). At the computational specification level, these parameters define a sub-type of usual numerical types, yielding a better definition of the related code.

This concerns numerical sensory data and internal quantitative data (e.g., derived data, calculation output, etc). A step further, symbolically coded data can always be sampled (e.g., a vector font drawn on a canvas and then sampled as pixels) at a given precision.

It is obvious that any quantitative measure is bounded (e.g. physical velocity magnitude stands between 0, for a still object, and the light speed) and is given up to a given precision (e.g., a localization in an image is given up to one-pixel size, a school ruler up to 1-millimeter graduation). The key point is that it is useful to make explicit this obvious fact (e.g., that any measurement device has a given precision and a measurement range) at the specification level instead of using it implicitly when required.

The notion of the positive sampling step, in order to define a local neighborhood size, is used to weight distance calculation, and to properly sample the data space: The underlying idea is that the state space is locally convex so that in a given neighborhood local search of an optimum yields to the optimal local value. This idea has been implemented in the \href{https://line.gitlabpages.inria.fr/aide-group/stepsolver}{stepsolver} variational solver, i.e., optimizer and controller.

This specification induces a \href{https://en.wikipedia.org/wiki/Pseudometric_space}{pseudometric}:
\eqline{d(x, x') = \frac{|x - x'|}{step}, |x - x'| > precision \Rightarrow x \neq x',}
in words, the distance is weighted by the $step$, i.e., the neighborhood approximate size, while if two differ by a quantity below the precision threshold, they are indistinguishable (thus either equal or not), so that we can decide if two values are different but not decide about their equality.

\paragraph{Value normalization}~\\

An important point is to be able to renormalize, say in the $[-1, 1]$ interval, the value for numerical stability, heterogeneous quantities comparison, etc. If the $zero$ value is not to be set to $0$, then an obvious linear transformation is sufficient.

Otherwise, given bounds $[min..max]$ and a default value $zero$, with $min < zero < max$, the transform\footnote{Here $signum(x) \deq x/|x|$ with $signum(0) \deq 0$.} $u_\infty(x) \deq \lim_{n \rightarrow +\infty} u_n(x)$
\eqline{\begin{array}{rclrcl}
 u_0 &\leftarrow& \frac{2 \, x - (min + max)}{max - min}, &
     z_0 &\deq& \left. u_0 \right|_{x = zero}, \\
 u_{n+1} &\leftarrow& u_n - \frac{\tilde{z}_n}{1 - \tilde{z}_n^2} \, (1 - u_n^2), &
     \tilde{z}_n &\deq& \min(z_{n-1}, \mbox{signum}(z_0) \, (\sqrt{2} - 1)), \\
     &z_{n+1} &\deq& \left. u_{n+1} \right|_{u_n = z_n}, \\
 \end{array}}
is monotonic and convergent with $z_{n+1} = 0$ after a finite number of iteration as soon as $|z_n| < (\sqrt{2} - 1)$, allows to map $u_\infty(min) = -1$, $u_\infty(max) = 1$, $u_\infty(zero) = 0$, corresponds to a simple linear scaling if $zero = (min + max)/2$ and a quadratic scaling if $|z_0| \leq |\sqrt{2} - 1|$, up to a polynomial of degree 16 (thus height iterations) when at about 1\% of a bound, as verified by obvious symbolic derivation\footnote{See \url{https://line.gitlabpages.inria.fr/aide-group/symboling/numeric-normalize.mpl} for the derivation source.}.

This mechanism allows one to map on the $[-1, 1]$ interval with $0$ as the default value. Unless $zero = (min + max) / 2$, the transformation is not linear as shown in Fig.~\ref{normalization}, with some incidence of the step and precision values.

\begin{figure}[htb]
 \centerline{\begin{tabular}{cc}
   \includegraphics[width=0.4\textwidth]{./normalization-1.png} &
   \includegraphics[width=0.4\textwidth]{./normalization-2.png}
 \end{tabular}}
 \caption{Numerical value normalization taking the $zero$ default value into account. Left: $zero < (min + max) / 2$; the design choice induces that the profile is almost linear below the zero value, allowing the precision and step estimation to be stable, while flattened after, thus reducing both precision and step values. Right $zero < (min + max) / 2$, with dual observation.}
 \label{normalization}
\end{figure}

\vspace*{-0.05in} \subsection{On symbolic to numeric information coding} \vspace*{-0.05in} \label{symbonumerication}

This numerical type being well specified, let us review a few elements regarding the transformation of symbolic data into numeric one, as an alternative to the proposed methodology.

At the basic information coding level, already, there is a duality between numeric versus symbolic representation \cite{riley_computational_2014}. As an example, a picture can either be encoded numerically as an array of pixels or symbolically encoded (i.e., as a vectorial picture) through a sequence of graphic items in correspondence with drawing instructions; this is  the case of raster versus vector fonts. Music is numerically encoded when sampled in an audio file and symbolically represented when written on a score. Numeric representation is, by construction, defined up to a certain precision, but is able to represent complex information, e.g., any image or any sound, without limitation, but the approximation. Symbolic representation is by construction exact, but limited to what the formal language allows to define.

\begin{figure}[htb]
 \centerline{\includegraphics[width=0.85\textwidth]{./samplingornot.png}}
 \caption{Illustration of the symbolic versus numeric representation of visual or audio data. A circle can be specified either at a symbolic level (here using the SVG specification in wJSON syntax), mainly providing parameters to a procedure allowing one to draw it, or a matrix of pixels. A sound can be either specified using a music sheet or as a set of audio samples. (The bottom left image comes from Wikipedia and the bottom right image is a crop of the Audacity software panel.)}
 \label{samplingornot}
\end{figure}

In an input-output system, typically a sensory-motor system, numerical data is provided by sensors on input and required by some actuators on output (some others like switches require binary values, while symbolic data is mostly related to a-priory information and higher-level internal representation of the system. 

Obviously, but crucial, this has some consequences. 

Symbolic information extraction from numerical data is a complex challenge, both at the specification level, i.e., to properly define, and at the computation level: In a sense, most of the machine learning algorithms (e.g., object category recognition in an image, or audio voice to text translation) aim at extracting some symbolic information from numerical data. This will not be developed here, while we focus on symbolic-to-numeric mapping and how to perform symbolic operations numerically.

A symbolically coded data can always be sampled (e.g., a vector font drawn on a canvas and then sampled as pixels) at a given precision, this corresponds to the specification proposed here.

Sampling makes sense only if associated with some parameters. The precision level and measure range are to be defined. Such specification is important to properly manipulate such information. In particular. From this specification, the values can be normalized (e.g., mapped onto the $[0, 1]$ interval) and mapped onto a finite set of relevant values. One consequence is that algorithm precision thresholds can be deduced (often using first-order approximations), spurious values can be detected, numerical conditioning of algorithms is enhanced, and so on (see \cite{vieville_implementing_2001} for a discussion). At the computational specification level, these parameters define a sub-type of usual numerical types, yielding a better definition of the related code. This also concerns numerical sensory data and internal quantitative data (e.g., derived data, calculation output, etc).

\paragraph{Multidimensional data representation}~\\

When considering multidimensional numerical quantities, e.g., a \href{https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions}{3D rotation}, as illustrated if Fig~\ref{3d-rotation}, the choice of the representation is application dependent and is to be related to the underlying mechanism to be modeled.

\begin{figure}[htb]
 \centerline{\includegraphics[width=0.6\textwidth]{./3d-rotation.png}}
 \caption{A visualization of a rotation represented by an unary vector $\hat{\mathbf e}$ specifying the rotation axis and an angle $\theta$. From wiki-commons. The rotation matrix writes \\\centerline{$\mathbf{R} =
  e^{\theta \, [\hat{\mathbf{e}}]_\times} =
  \mathbf{I} +\sin(\theta) \, [\hat{\mathbf{e}}]_\times + (1-\cos(\theta)) \, [\hat{\mathbf{e}}]_\times^2$}\\ where the skew-symmetric matrix $[\hat{\mathbf{e}}]_\times$ is defined by $[\hat{\mathbf{e}}]_\times \, \mathbf{x} = \mathbf{e} \times \mathbf{x}$ where $\times$ stands for the cross-product. Choosing to represent a rotation by a unary vector, i.e., a direction, and an angle instead of, e.g., the matrix components, is usually more representative of the underlying physical mechanism.}
 \label{3d-rotation}
\end{figure}

For this example, the 3D rotation makes use of a unary vector, in which distance is usually given by the cosine similarity, i.e.,
\eqline{d(\hat{\mathbf{e}}, \hat{\mathbf{e}}') \deq \hat{\mathbf{e}} \cdot \hat{\mathbf{e}}' =
 1 - \cos\left(\widehat{\hat{\mathbf{e}}, \hat{\mathbf{e}}'}\right) \in [0, 2]}
where $\cdot$ stands for the dot product, which generalizes the scalar distance discussed in this section. In such a case bounds $min = -1$ and $max = 1$ applies to the unary vector components, while the precision applies on the cosine distance, and the step is a more complex specification of the \href{https://en.wikipedia.org/wiki/Tessellation}{sphere tessellation}.

A step further, when considering qualitative quantities to be embedded in a multidimensional numerical space, again the translation to numerical bounded multidimensional values is application dependent in the sense that the embedding induces semantic properties. For instance, embedding qualitative values onto a mono-dimensional numerical axis means that we consider these quantities as a sequence of values are equal distances or not. This is illustrated in Fig~\ref{color-parameterization} for a simple example of \href{https://en.wikipedia.org/wiki/Color_space}{color space}.

\begin{figure}[ht]
 \includegraphics[width=0.9\textwidth]{color-parameterization.png}
 \caption{Let us consider the numerical grounding of a few colors, say, blue (B), green (G), red (R) and white (W). If we simply index these colors with positive numbers, 1, 2, and so on (on the left), we implicitly introduce a sequential structure (e.g., that green is ``before'' red), here related to the English words alphabetic order, which is very likely not relevant and will definitely introduce biases when manipulating the numbers. If we embed these symbols in a vectorial space of dimension 4, with vectors such that $\protect\overrightarrow{blue} = (1, 0, 0, 0)$ up to $\protect\overrightarrow{white} = (0, 0, 0, 1)$, or on an isotropic tetrahedron in dimension 3 (in the middle), then the information is homogeneous since all colors are at equal distances, allowing us to define intermediate colors as linear combinations. We may also embed such symbols in a higher dimensional space. However, this does not really correspond to a relevant color space in link with either a physical or psychometric perception chromaticity representation, shown on the right. This example illustrates that qualitative information coding design choices are in deep link with a priori information considered at the problem specification.}
 \label{color-parameterization}
\end{figure}

At a higher level of generality, as developed in Appendix~\ref{vsa-implementation}, embedding qualitative $K$ values with predefined distances in a numerical vector space of dimension at least $K$ is straightforward.

\paragraph{Logical information numerical encoding}~\\

A step further, logical information can be formally grounded on a numerical vectorial space, using a localist representation, such as in \cite{badreddine_logic_2021}, \cite{riegel_logical_2020} or \cite{guo_jointly_2016}. Each symbol is mapped onto a high dimensional vector of dimension $10^{2\cdots3}$, each functional term corresponds to a vectorial function and relations to a function mapped onto $[0, 1]$ with the interpretation that the relation is true if the value is $1$ and false if the value is $0$, introducing real-valued logic. This allows to extends usual symbolic Boolean operators to numerical operations: In most cases the not operator $not(u) \deq 1 - u$ which is compatible with the true and false numerical values, the other logical operators being associated to a T-norm, e.g., $u \mbox{ and } v = u \, v$ (product T-norm) or $u \mbox{ and } v = \min(u, v)$ (fuzzy logic T-norm), etc. Such construction allows a first-order logic specification to be properly rewritten as a system of numerical equations. It also allows us to introduce the notion of being ``approximately true'' with different interpretations, either analog to probabilistic interpretation or closer to modal logic. Given such specification, and a set of facts, i.e., a set of relations applied on constants, the problem of optimizing the numerical representation, for instance, adjusting the constant vectorial value and function and relation parameters choosing for instance multi-linear parameterization (as in logic tensor network) is well-defined \cite{badreddine_logic_2021,guo_jointly_2016}. This also allows to perform logical inference at the numerical level \cite{riegel_logical_2020}, including real-logic inference \cite{guo_jointly_2016}. An alternative mapping of symbolic information onto a numerical framework is provided by semi-algebraic geometry yielding to semi-definite programming \cite{netzer_real_2016}.

\subsubsection*{Symbolic information numerical encoding}~\\

When considering weak defined structured data, such as natural language written sentences (see, e.g., \cite{rong_word2vec_2016} for an introduction and review) or data representation of hierarchical human knowledge (e.g., \cite{xu_understanding_2021}) each symbol is translated in a high dimensional space. These coordinate vector values are adjusted using coding decoding methods, to optimize the representation that minimizes the decoding error. For sentences of words, word co-occurrences are considered as relevant cues, and the cross-entropy between input and output distributions of such co-occurrence is optimized, using neural network-based encoders and soft-max or hierarchical soft-max output prediction layers, while negative sampling of counterexamples and ad-hoc treatments (such as managing too frequent small words such as articles) are also used. A dictionary of a few hundred to thousand words is embedded in a numerical space of hundred to thousands of variables, and the metric of this vectorial space statistically reflects the words co-occurrences, which is interpreted as semantic proximity \cite{rong_word2vec_2016}. For more complex data structures modeled as graphs, based on the same principle of vectorial encoding such that the dual decoding statistically predicts the original data structure, measures on the graph are often random walks over the graph paths (much more efficient than considering explicitly graph adjacency), while the optimization criterion is either purely statistics (e.g., mutual information) or structural (e.g., semantic distances between the input and predicted graph) \cite{xu_understanding_2021}. In such a case, the embedding is no more based on a-priory information, but on numerically estimated statistical regularities.

Reviewing these elements allows us to show the strong difference in neurosymbolic approaches as discussed in sub-section~\ref{neurosymbolic}.


