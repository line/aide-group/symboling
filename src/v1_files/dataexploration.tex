
In section~\ref{extrapolation} the extrapolation along a path from a current value with respect to another reference value, using data type bounds has been developed. Let us draft the more general problem of data structure exploration. This is only discussed as a ``bag of ideas'' to explore the expressive power of the present specifications, concrete implementation being beyond the scope of this work.

\vspace*{-0.05in} \subsection{Generating other values from a dictionary} \vspace*{-0.05in} \label{generation} 

Given a scalar field with predefined values or a data type with bounds, the 1st idea is to consider these as a ``dictionary'' of possible values, and paths from these values to the current value ${\bf s}_0$ and select these paths:
\\- [closest] The closest data structure different from ${\bf s}_0$.
\\- [remote] The data structure at the highest distance from ${\bf s}_0$.
\\- [random] A random data structure different, from ${\bf s}_0$.
or a
\\- [user-defined reward] Selecting a value that approximately maximizes a user-defined or predefined reward, as it is the case for the trajectory generation.
\\- [user-defined selection mechanism] Such a mechanism can be for instance a random selection, given a user-defined distribution.
\\ We could also have considered the remote value at the highest distance below a given bound $d_{\max}$ or the closest value above a given bound $d_{\min}$ or random value in a given $[d_{\min}, d_{\max}]$ interval. This seems to be a reasonable set of interesting alternatives that can be designed, given only distances and geodesic.

A step further, if we have a default value, in the sense that it is the value as closed as possible to an undefined value, we can select:
\\- [smaller] The closest data structure different from ${\bf s}_0$, with a lower distance to the the default value than ${\bf s}_0$.
\\- [larger] The closest data structure different from ${\bf s}_0$, with a higher distance to the the default value than ${\bf s}_0$.
\\- [smallest] The data structure at the highest distance from ${\bf s}_0$, with a lower distance to the default value than ${\bf s}_0$.
\\- [largest] The data structure at the highest distance from ${\bf s}_0$, with a higher distance to the default value than ${\bf s}_0$.

Depending on the chosen criterion, this is in the worst case performed at a linear cost with respect to the scalar field value data size and related path lengths.

\vspace*{-0.05in} \subsection{Hierarchical generation of a new value from a reference one} \vspace*{-0.05in}
 
Another track is to see to what extent we can generate a new value given a current value ${\bf s}_0$, without any dictionary. Let us first consider two simple types: enumeration, as implemented here\footnote{\url{https://line.gitlabpages.inria.fr/aide-group/symboling/EnumType.html}} and numeric types, as implemented here\footnote{\url{https://line.gitlabpages.inria.fr/aide-group/symboling/NumericType.html}}.

{\em Generating a new value among an enumeration.} In that case, we are in the same situation as when considering the scalar field values, except that we do not consider the values on the path but only the values themselves.

{\em Generating a new value for a bounded numeric value.} In the simple case of a numeric value, the previous alternatives of closest, remote, uniform random, or extrapolation are obvious to implement, and will not be detailed, please refer to the numeric data type, as implemented here\footnote{\url{https://line.gitlabpages.inria.fr/aide-group/symboling/numerictype.pdf}}. Here the closest value is the value at a distance of the precision $\epsilon$, and depending on the criterion we may have to randomly choose the smaller or larger value, which is also the case if both bounds are at equal distance and a remote value is chosen.

Beyond these two scalar cases, let us now discuss to what extent we can define a new value considering the three constructions of set, list, and record.

{\em Generating the closest value, given a compound of value.} For a set, list, or record, we generate a new value by modifying, inserting, or deleting a new element of the set and generating a {\em closest} value requires to perform one of these operations. Thus either an insertion, of the default value of the element type, because this is the closest value to an undefined (i.e., here nonexistent) value, or modify one of the element values, modifying the element considering the closest new element value. This includes deletion, which is nothing but replacing it with an undefined value if it is the closest value. Doing more than one modification can not minimize the distance to the new value, because it is additive in this case. The complexity of such an operation is linear with the set side, and implementation is tractable.

However, defining the {\em remote} new value (i.e. a new value at a maximal distance) is meaningless here, because we always can insert an unbounded number of new values increasing the distance at will. Then, defining a remote value below a given threshold, or the closest value above a given threshold, is much more complex, because we have to consider the combination of modification of several, when not all, elements and then test all combinations so that up to our best understanding, this is not tractable. If we do not consider element insertion or deletion, but only modification, the situation is still complex, because choosing for each element a value at maximal distance does not mean that the resulting set will be at maximal distance, because new lower distances between elements could be found. We thus will not implement remote new value for a set.

{\em Generating a random value, given a compound of value.}  It is more simple to define a {\em random} new value, defining the probability to delete an element, inserting a random element, or inserting or modifying a random field value in a record. This however highly depends upon the probability distribution, which is to be specified by the user.

{\em Generating a new value, given a record.} For a record, in our context, we consider that the list of named items is fixed, i.e., do not consider inserting a new unexpected item, while deleting an item, simply means replacing the value by a default value. Given this restriction, the distance being additive on each item, we easily can define the closest value by modifying only one item whose closest value distance is minimum, a remote value by considering each item's remote value, if defined (i.e. if it is an enumeration, a numerical value or a record), a random value by randomly modifying each item value, if above a random threshold.
