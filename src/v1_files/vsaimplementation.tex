Let us consider a list of $K$ qualitative values, i.e. symbols in the restrained sense, equipped with a metric, i.e., distance values between each pair of symbols. We develop here the fact that we can easily embed them in a vectorial\footnote{It would also have been easy to show that it can be embedded in an affine space of dimension $N \ge K-1$ with a similar demonstration, left to the reader.} space of dimension $N \ge K$. It appears that the mechanism is a simple diagonalization of the symmetric positive distance matrix, followed by a symmetric orthogonalization algorithm, thus a straightforward process. We also see that, given a first vector, this decomposition is generic, i.e., unique up vector permutation and rotation.

In practice, this means that embedding qualitative values with predefined distances in a numerical space is straightforward.

\paragraph{Unary vector placement at given distances}~\\

Let us consider a set of $K$ unary vectors $\{\cdots {\bf x}_k \cdots\}$ in ${\cal R}^N$ written in $N \times K$ matrix form ${\bf X} = \left(\cdots {\bf x}_k \cdots\right)$ of $N$ rows and $K$ columns, while $\|{\bf x}_k\| = 1$.

The goal is to place these vectors at predefined distances $d_{kl}$, i.e. such that:
\eqline{\|{\bf x}_k - {\bf x}_l \| = 2 - 2 {\bf x}_k^T \,{\bf x}_l = d_{kl}, \;\;\; \Leftrightarrow {\bf x}_k^T \,{\bf x}_l = c_{kl}, \mbox{ with } c_{kl} \deq 1 - 2 \, \left(\frac{d_{kl}}{\max(d_{kl})}\right)^2, \mbox{ and } c_{kk} = 1}
with since these unary vectors live on a unit hypersphere, thus for which the distance is maximal if separated by a diameter of length $2$, while $c_{kl} \in [-1, 1]$ with this normalization.

\paragraph{From distance placement to orthogonalization}~\\

This constraint writes in matrix form ${\bf X}^T \, {\bf X} = {\bf C}$, where ${\bf C}$ is the $K \times K$ symmetric positive matrix of component $c_{kl}$, while we can define:
\eqline{{\bf C} = {\bf R}^T \, {\bf \Lambda} \, {\bf R} = {\bf S}^T \, {\bf S}, \mbox{ with } {\bf S} = \sqrt{\bf \Lambda} \, {\bf R}}
where ${\bf R}$ is an orthogonal $K \times K$ matrix with ${\bf R}^{-1} = {\bf R}^T$ and ${\bf \Lambda}$ is a $K \times K$ diagonal positive matrix, thus with $\lambda_k > 0$, while $\sqrt{\bf \Lambda}$ corresponds to a diagonal matrix with the square root values on the diagonal. Such decomposition is easily obtained by \href{https://en.wikipedia.org/wiki/Diagonalizable_matrix#Diagonalizable_matrices}{diagonalization} or using \href{https://en.wikipedia.org/wiki/Singular_value_decomposition}{singular value decomposition}.

The key point is that:
\eqline{{\bf X}^T \, {\bf X} = {\bf C} \Leftrightarrow {\bf Z}^T \, {\bf Z} = {\bf I}, \mbox{ writing } {\bf X} = {\bf Z} \, \sqrt{\bf \Lambda} \, {\bf R}}
in words, finding a set of unary vectors at predefined distances is equivalent of finding a set of orthonormal vectors, up to leftward $K \times K$ linear transformation, as easily verified from obvious algebra. Moreover, the obtained vectors are as ``orthogonal as possible'' in the following sense that they solve the optimization\footnote{In order to verify this last statement, let us consider ${\bf Y} \deq {\bf X} \, {\bf R}^T$, so that the problem reduces to:
\eqline{min |{\bf Y}^T \, {\bf Y} - {\bf I}|^2, {\bf Y}^T \, {\bf Y} = {\bf \Lambda}}
by the virtues of orthogonal matrices. This totally decouples the equations and we are left to find orthogonal vectors $\{\cdots {\bf y}_k \cdots\}$ such that $\|{\bf y}_k\|^2 = \lambda_k$ instead of $1$. The shortest transformation is obviously ${\bf y}_k \leftarrow \sqrt{\lambda_k} \, {\bf y}_k$ preserving orthogonality and solving the problem.} problem:
\eqline{min |{\bf X}^T \, {\bf X} - {\bf I}|^2, {\bf X}^T \, {\bf X} = {\bf C}.}

\paragraph{Drawing random unary vectors}~\\

This corresponds to the \href{https://en.wikipedia.org/wiki/Orthogonalization#Orthogonalization_algorithms}{symmetric orthogonalization algorithm} where given a set of original vectors $\{\cdots \bar{\bf x}_k \cdots\}$ with $\bar{\bf x}_k^T \, \bar{\bf x}_l = \bar{c}_{kl}$ the transformation ${\bf X} \leftarrow \bar{\bf R}^T \, \sqrt{\bar{ \bf \Lambda}}^{-1} \, \bar{\bf X}$ yields ${\bf X}^T \, {\bf X} = {\bf I}$ as easily verified.

As an alternative, considering random vectors $\{\cdots \bar{\bf z}_k \cdots\}$ in a space of large dimension $N \gg K$ so that they are almost orthonormal, we directly solve the problem in close form writing ${\bf X} \leftarrow {\bf Z} \, \sqrt{\bf \Lambda} \, {\bf R}$.




