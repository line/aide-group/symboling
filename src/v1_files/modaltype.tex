
The Boolean notion of being either false or true is generalized here as a numeric representation of partial knowledge, considering a value $\tau \in [-1, 1]$, as illustrated in Fig.~\ref{possibility-necessity}.

\begin{figure}[htb]
 \centerline{\includegraphics[width=0.85\textwidth]{./possibility-necessity.png}}
 \caption{Specification of a modal value of belief, this with regard to necessity and possibility, as defined in the possibility theory. The interpretation is that what is ``not so false'' is partially possible but not necessary and what is ``partially true'' is entirely possible but partially necessary. Such a formulation corresponds qualitatively to the human appreciation of the degree of belief in a fact.}
 \label{possibility-necessity}
\end{figure}

The true value corresponds to 1 (fully possible and fully necessary), the false value to -1 (neither possible nor necessary, i.e., impossible), and the unknown value to 0, which corresponds to a fully possible but absolutely not necessary value\footnote{This representation has been designed to be compatible with the ternary Kleene logic, and is in one to one correspondence with respect to the possibility theory. Possibility theory is devoted to the modeling of incomplete information, in link with an observer's belief regarding a potential event and surprise after the event occurrence (please refer to \cite{denoeux_representations_2020} for a general introduction). While almost all partially known information is related to probability, human “level of truth” is more subtle and related to possibility and necessity, as formalized in the possibility theory, as discussed in \cite{denoeux_representations_2020} and \cite{denoeux_representations_2020-1}, in link with modal logic, i.e., something true in a “given context” \cite{fischer_modal_2018}, which is also considered as representative to what is modeled in educational science and philosophy \cite{rusawuk_possibility_2018}, because it corresponds to commonsense reasoning in the Piaget's sense \cite{smith_development_1994}, taking exceptions into account, i.e., considering non-monotonic reasoning. Furthermore, in symbolic artificial intelligence, i.e., knowledge representation through ontology, the link has been built between this necessity/possibility dual representation and ontology \cite{tettamanzi_possibilistic_2017}. This must be understood as a deterministic theory, in the sense that partial knowledge is not represented by randomness. See \cite{vallaeys_generaliser_2021,vieville_representation_2022} for an extension taking randomness into account.}. In between, negative values correspond to partially possible events and positive values to partially necessary events.

Boolean true/false values and Kleene three value logic true/unknown/false values conjunction ({\tt and}), disjunction ({\tt or}), exclusive disjunction ({\tt xor}) and element set difference generalizations correspond to $\min$, $\max$, and polynomial operators.

\begin{figure}[htb]
\centerline{\includegraphics[width=0.4\textwidth]{./possibility-projection.png}}
\caption{Possibility projection, see text for details} \label{possibility-projection}
\end{figure}

Our representation $\tau \in [-1,1]$ is in correspondence with modal logic, given possibility $\pi$, and necessity $\nu$, with $0 \le \nu \le \pi \le 1$ by construction:
\eqline{
\left\{\begin{array}{rclcl}
\tau &\deq& \nu + \pi - 1 &\in& [-1, 1]\\
\rho &\deq& \nu - \pi + 1 &\in& [0, 1]\\
\end{array}\right.
\mbox{ with } \left\{\begin{array}{rclcr}
\pi &=& \frac{\tau - \rho}{2} + 1 &=& \left. 1 + H(-\tau) \, \tau \right|_{\nu \, (\pi - 1) = 0} \\
\nu &=& \frac{\tau + \rho}{2} &=& \left. \;\;\; H(\tau) \, \tau  \right|_{\nu \, (\pi - 1) = 0} , 
\end{array}\right.}
In the standard deterministic case \cite{denoeux_representations_2020} the correspondence is a one to one, because:
\eqline{\nu > 0 \Rightarrow \pi = 1 \mbox{ and } \pi < 1 \Rightarrow \nu = 0 \mbox{, thus } \nu \, (\pi - 1) = 0}
this is a 1D representation with $\rho = \pm \tau$, where either $\pi$ or $\nu$ varies, corresponding to the thick segment line of the figure. \\ In the generalized case \cite{vallaeys_generaliser_2021,vieville_representation_2022} $\nu$ and $\pi$ bound a probability $p$, i.e., $\nu \le p \le \pi$. If $\nu = \pi$ we are in a pure random case with a known probability estimation $p = (\tau+1)/2$. \\ We also can define the projection of any value $(\pi, \nu)$, using:
\eqline{\tau \leftarrow \max(-1, \min(1, \nu + \pi - 1)), \rho \leftarrow \max(|\tau|, \min(1, \nu - \pi + 1))}
which partition the $(\pi, \nu)$ space in four oblique regions of false, partially possible, partially necessary, and true regions. 
