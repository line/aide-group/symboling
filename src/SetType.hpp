#ifndef __symboling_SetType__
#define __symboling_SetType__

#include <math.h>
#include "ListType.hpp"

namespace symboling {
  /**
   * @class SetType
   * @description Specifies a set type, i.e. a unordered non-repetitive set of elements.
   * @extends Type
   *
   * <a name="parameters"></a>
   * ## Type parameters
   * ### General parameters
   * - `name: ...` The type name, if specific. By default the type name is `set-of-$itemType`.
   * - `itemType: ...` The type elements if any. By default, "value", means any value.
   * ### Parameters for the syntactic projection
   * - `minLength: ...` The minimal number of items in the list. By default `0`.
   * - `maxLength: ...` The maximal number of items in the list. By default unbounded (`-1`).
   * ### Parameters for the cost calculation
   * - `deleteCost: ...` The non-negative deleting cost value.
   *   - By default uses the `getDistance()` between the item and an empty data structure.
   * - `insertCost: ...` The non-negative inserting cost value.
   *   - By default equals to the `deleteCost`.
   * - `editCost: ...` The non-negative editing cost value.
   *   - By default uses the `getDistance()` between each item.
   *
   * <a name="specification"></a>
   * ## SetType specification
   * <table class="specification">
   * <tr style='border: 1px solid black;'><th> Syntactic projection </th><td>
   * - If the value is empty, it corresponds to an empty set.
   * <br/> - If the value is not a set, it is encapsulated in a <tt>[ value ]</tt> singleton.
   * <br/> - If the set length is not in the (optional) <tt>[minLength, maxLength]</tt> bounds a message is issued, but the set is neither truncated nor extended.
   * <br/> - Each set item is syntactically projected.
   * <br/> - In order to be comparable the set is put in a canonical form:
   * <br/> &nbsp;&nbsp;&nbsp;&nbsp; - Set elements are sorted according to the item type comparison.
   * <br/> &nbsp;&nbsp;&nbsp;&nbsp; - Redundant elements that compare to 0 with respect to another are erased.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Semantic projection  </th><td>
   * - Each set element is semantically projected.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Distance value       </th><td>
   * - The minimal distance is computed using the <a href="https://github.com/mcximing/hungarian-algorithm-cpp">Cong Ma, 2016</a> matrix version of the <a href="https://en.wikipedia.org/wiki/Hungarian_algorithm">Hugarian algorithm</a>.
   * <br/> &nbsp;&nbsp;&nbsp;&nbsp; - The algorithmic complexity order of magnitude is thus cubic with respect to the set size considering the item type as basic operation.
   * <br/> - The editing distance can be either:
   * <br/> &nbsp;&nbsp;&nbsp;&nbsp; - parameterized defining fixed deletion, insertion and editing costs, or
   * <br/> &nbsp;&nbsp;&nbsp;&nbsp; - parameterized rederiving the <a href="./ListType.html#cost">cost function</a>.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Geodesic path        </th><td>
   * - The geodesic path is based on the Hugarian algorithm assignment map which is scanned in the assignment list order considering the minimal cost operation of deletion, insertion or replacement.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Comparison           </th><td>
   * The comparison is applied on the set canonical form.
   * - Returns <tt>0</tt> If each element pair compares equal and both lists have the same length.
   * <br/> - Returns <tt><0</tt> If either the value of the first item that does not match is lower in the left hand side list, or all compared items match but the left hand side list is shorter.
   * <br/> - Returns <tt>>0</tt> If either the value of the first item that does not match is greater in the left hand side list, or all compared items match but the left hand side list is longer.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Bounds           </th><td>
   * - Considers all list lengths beetwen `minLength` and `maxLength` to calculate the bounds, from the item type bounds.
   * </td></tr>
   * </table>
   *
   * @param {JSON|String} parameters The type parameters.
   */
  class SetType: public ListType {
    mutable std::vector < std::vector < double >> costMatrix;
    mutable std::vector < int > assignment;
public:
    SetType(JSON parameters);
    SetType(String parameters);
    SetType(const char *parameters);
    SetType();

    virtual wjson::Value getValue(JSON value, bool semantically_else_syntactically = true) const;
    virtual double getDistance(JSON lhs, JSON rhs) const;
    virtual wjson::Value getPath(JSON lhs, JSON rhs) const;
    virtual String asString() const;
  };
}

#endif
