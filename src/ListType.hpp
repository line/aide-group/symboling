#ifndef __symboling_ListType__
#define __symboling_ListType__

#include <math.h>
#include "Type.hpp"

namespace symboling {
  /**
   * @class ListType
   * @description Specifies a list type, i.e. an strictly ordered container of elements.
   * @extends Type
   *
   * <a name="parameters"></a>
   * ## Type parameters
   * ### General parameters
   * - `name: ...` The type name. By default the type name is `list-of-$itemType`.
   * - `itemType: ...` The type elements if any. By default, "value", means any value.
   * ### Parameters for the syntactic projection
   * - `minLength: ...` The minimal number of items in the list. By default `0`.
   * - `maxLength: ...` The maximal number of items in the list. By default unbounded (`-1`).
   * ### Parameters for the cost calculation
   * - `deleteCost: ...` The non-negative deleting cost value.
   *   - By default uses the `getDistance()` between the item and an empty data structure.
   * - `insertCost: ...` The non-negative inserting cost value.
   *   - By default equals to the `deleteCost`.
   * - `editCost: ...` The non-negative editing cost value.
   *   - By default uses the `getDistance()` between each item.
   *
   * <a name="specification"></a>
   * ## ListType specification
   * <table class="specification">
   * <tr style='border: 1px solid black;'><th> Syntactic projection </th><td>
   * - If the value is empty, it corresponds to an empty list.
   * <br/> - If the value is not a list, it is encapsulated in a <tt>[ value ]</tt> singleton.
   * <br/> - If the list length is not in the (optional) <tt>[minLength, maxLength]</tt> bounds a message is issued, but the list is neither truncated nor extended.
   * <br/> - Each list item is syntactically projected.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Semantic projection  </th><td>
   * - Each list item is semantically projected.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Distance value       </th><td>
   * - The minimal distance is computed using the <a href="https://en.wikipedia.org/wiki/Levenshtein_distance">Levenshtein distance</a> calculation applied on the list element and delegating the edition cost to the item Type distance computation.
   * <br/> &nbsp;&nbsp;&nbsp;&nbsp; - The algorithmic complexity order of magnitude is thus quadratic with respect to the list length considering the item type complexity as basic operation.
   * <br/> - The editing distance can be either:
   * <br/> &nbsp;&nbsp;&nbsp;&nbsp; - parameterized defining fixed deletion, insertion and editing costs, or
   * <br/> &nbsp;&nbsp;&nbsp;&nbsp; - parameterized rederiving the <a href="#cost">cost()</a> function.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Geodesic path        </th><td>
   * - The geodesic path corresponds to the <a href="https://en.wikipedia.org/wiki/Wagner%E2%80%93Fischer_algorithm">Wagner-Fischer algorithm</a> shortest path in the matrix distance yielding to the minimal distance computation.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Comparison           </th><td>
   * - Returns <tt>0</tt> If each element pair compares equal and both lists have the same length.
   * <br/> - Returns <tt><0</tt> If either the value of the first item that does not match is lower in the left hand side list, or all compared items match but the left hand side list is shorter.
   * <br/> - Returns <tt>>0</tt> If either the value of the first item that does not match is greater in the left hand side list, or all compared items match but the left hand side list is longer.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Bounds           </th><td>
   * - Considers all list lengths beetwen `minLength` and `maxLength` to calculate the bounds, from the item type bounds.
   * </td></tr>
   * </table>
   *
   * @param {JSON|String} parameters The type parameters.
   */
  class ListType: public Type {
protected:
    const Type& itemType;
    double deleteCost, insertCost, editCost;
    unsigned int minLength, maxLength;
#ifndef SWIG
    // Editing distance variables
    struct Operation {
      char what = '?';
      unsigned int i = -1, j = -1;
      double cost = 0;
      Operation() {}
      Operation(char what, unsigned int i, unsigned int j, double cost) : what(what), i(i), j(j), cost(cost) {}
    };
    mutable Operation *d = NULL;
#endif
    mutable unsigned int ni, nj, nij, *ij_path = NULL;
public:
    ListType(JSON parameters);
    ListType(String parameters);
    ListType(const char *parameters);
    ListType();
    virtual ~ListType();

    virtual wjson::Value getValue(JSON value, bool semantically_else_syntactically = true) const;
    virtual double getDistance(JSON lhs, JSON rhs) const;
    virtual wjson::Value getPath(JSON lhs, JSON rhs) const;
    virtual double compare(JSON lhs, JSON rhs) const;
    virtual JSON getBound(unsigned int index = 0) const;
private:
    double log_c;
    unsigned int *ipow_c = NULL;
public:
    virtual String asString() const;

    /**
     * @function cost
     * @memberof ListType
     * @instance
     * @description Cost function of the editing distance
     * - The `ListType::cost()` implements the `deleteCost`, `insertCost` and `editCost` parameters related cost.
     * - A typical re-implementation is of the form:
     * ```
     * class MyListType : public ListType {
     * ../..
     *   double cost(char what, JSON lhs, unsigned int i, JSON rhs, unsigned int j) const {
     *    // Implements the MyListType specific features, if appropriate and returns the value
     *    return ListType::cost(what, lhs, i, rhs, j); // Delegates to the parent implementation otherwise
     *  }
     * }
     * ```
     * @param {char} what The editing operation:
     * - 'd' : deletion cost for the `lhs[i]` value.
     * - 'i' : insertion cost for the `rhs[j]` value.
     * - 'e' : edition cost to transform the `lhs[i]` to `rhs[j]`
     * @param {Value} lhs The left hand-size value.
     * @param {uint} i The lhs index.
     * @param {Value} rhs The right hand-size value.
     * @param {uint} j The rhs index.
     * @return {double} The cost value.
     */
    virtual double cost(char what, JSON lhs, unsigned int i, JSON rhs, unsigned int j) const;
  };
}

#endif
