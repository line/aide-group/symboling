#include "ModalType.hpp"
#include "std.hpp"

namespace symboling {
  ModalType::ModalType(JSON parameters) : NumericType(parameters.clone().set("name", parameters.get("name", "modal")).set("parent", "numeric").set("min", -1).set("max", 1).set("step", parameters.get("step", 2)).set("atomic", parameters.get("atomic", true))), trinary(parameters.get("trinary", false)), binary(parameters.get("binary", false))
  {
    aidesys::alert(trinary && binary, "illegal-argument", "in symboling::ModalType::ModalType contradictory parameters '%s', the type can not be both binary and trinary", getParameters().asString().c_str());
  }
  ModalType::ModalType(String parameters) : ModalType(wjson::string2json(parameters))
  {}
  ModalType::ModalType(const char *parameters) : ModalType(wjson::string2json(parameters))
  {}
  ModalType::ModalType() : ModalType("")
  {}

  double ModalType::getValueAsDouble(JSON value, bool semantically_else_syntactically) const
  {
    startCheckMessage();
    double result;
    std::string s = value.asString();
    std::transform(s.begin(), s.end(), s.begin(), ::tolower);
    if(s == "true") {
      result = 1;
    } else if(s == "false") {
      result = -1;
    } else if(s == "unknown" || s[0] == '?') {
      if(semantically_else_syntactically && binary) {
        addCheckMessage(value, "not a binary false (-1) or true (1) value");
      }
      result = binary ? -1 : 0;
    } else {
      char *end;
      result = strtod(s.c_str(), &end);
      if(*end == '\0') {
        if(semantically_else_syntactically) {
          if(result < -1) {
            addCheckMessage(value, "out of bound");
            result = -1;
          } else if(result > 1) {
            addCheckMessage(value, "out of bound");
            result = 1;
          } else {
            if(trinary) {
              if(result != -1 || result != 0 || result != 1) {
                addCheckMessage(value, "not a trinary false (-1), unknown (0) or true (1) value");
              }
              if(result < -0.5) {
                result = -1;
              }
              if(result > 0.5) {
                result = 1;
              }
              result = 0;
            } else if(binary) {
              if(result != -1 || result != 1) {
                addCheckMessage(value, "not a binary false (-1) or true (1) value");
              }
              if(result > 0) {
                result = 1;
              }
              result = -1;
            }
          }
        }
      } else {
        addCheckMessage(value, "not a parsable number");
        result = binary ? -1 : 0;
      }
    }
    stopCheckMessage();
    return result;
  }
  double ModalType::getPi(double tau)
  {
    return tau > 0 ? 1 : tau <= -1 ? 0 : tau + 1;
  }
  double ModalType::getNu(double tau)
  {
    return tau <= 0 ? 0 : tau >= 1 ? 1 : tau;
  }
  double ModalType::getTau(double pi, double nu)
  {
    double result = nu + pi - 1;
    return result < -1 ? -1 : result > 1 ? 1 : result;
  }
  double ModalType::getRho(double pi, double nu)
  {
    double rho_min = fabs(getTau(pi, nu)), result = nu - pi + 1;
    return result < rho_min ? rho_min : result > 1 ? 1 : result;
  }
  double ModalType::getOp(char op, double tau_1, double tau_2)
  {
    switch(op) {
    case 'a':
      return std::min(tau_1, tau_2);
    case 'o':
      return std::max(tau_1, tau_2);
    case 'x':
      return -tau_1 * tau_2;
    case 'm':
      return tau_1 * (1 - tau_1 - tau_2 - tau_1 * tau_2) / 2;
    default:
      aidesys::alert(true, "illegal-argument", "in symboling::ModalType::getOp undefined operation op: '%c'", op);
      return 0;
    }
  }
}
