#ifndef __symboling_EnumType__
#define __symboling_EnumType__

#include <math.h>
#include "ListType.hpp"

namespace symboling {
  /**
   * @class EnumType
   * @description Specifies a enumeration type, i.e. an unordered non-repetitive enumeration of elements.
   * @extends Type
   *
   * <a name="parameters"></a>
   * ## Type parameters
   * ### General parameter
   * - `name: ...` The type name. Required field.
   * - `itemType: ...` The type elements around the prototypes, used to project onto the prototypes.
   *   - By default, "value", means any value, and the default value is undefined.
   * - `values: ...` The enumeration of the prototypes defining this type.
   *   - The first value is the `default` value, i.e., the value as closed as possible to an undefined value.
   *   - Each value is semantically projected on the item data type to guaranty the coherence of the operations.
   *
   * <a name="specification"></a>
   * ## EnumType specification
   * <table class="specification">
   * <tr style='border: 1px solid black;'><th> Syntactic projection </th><td>
   * - The syntactic projection is delegated to the item type.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Semantic projection  </th><td>
   * - The semantic projection corresponds of choosing the value of minimal distance with respect to the item type.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Distance value       </th><td>
   * - The minimal distance computation is delegated to the item type and applied on each value of value set, selecting the minimal distance.
   * <br/> - The complexity order of magnitude is thus linear with respect to the enumeration length considering the item type complexity as basic operation.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Geodesic path        </th><td>
   * - The geodesic calculation is delegated to the item type.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Comparison           </th><td>
   * - The comparison is delegated to the item type.
   * </td></tr>
   *  <tr style='border: 1px solid black;'><th> Bounds           </th><td>
   * - The values are also the bounds for this type.
   * </td></tr>
   * </table>
   *
   * @param {JSON|String} parameters The type parameters.
   */
  class EnumType: public Type {
    const Type& itemType;
    mutable double lastDistance = NAN;
public:
    EnumType(JSON parameters);
    EnumType(String parameters);
    EnumType(const char *parameters);
    EnumType();

    /**
     * @function getInterpolatedValue
     * @memberof EnumType
     * @instance
     * @description Interpolates a value with respect to one of the enumerated prototypes.
     * @param {JSON} value The input value.
     * @param {JSON} parameters The interpolation parameters.
     * - `p: ...`: select a prototype by index between `{0, P{`, where `P` is the prototype count, otherwise it is randomly selected.
     * - `q: ...`: select, by index, on the path between the current value and the prototype, the selected value.
     *   - `q = 0`: corresponds to the input value.
     *   - `q = 1`: corresponds to the element in the direction of the prototype, which is as close as possible to the input value.
     *   - `q >= Q-1`: corresponds to the prototype, where `Q` is the path length corresponding to the prototype.
     * - `r: ...`: select (if q is not defined), by relative position, on the path between the current value and the prototype, the selected value.
     *  - `r = 0.0`: corresponds to the input value.
     *  - `t = 1.0`: corresponds to the prototype.
     *  - `0 < r < 1`: select to an element at a relative position of the path, which is neither the current nor the prototype.
     * Otherwise select a random value on the path.
     * @return {Value} The interpolated value.
     */
    wjson::Value getInterpolatedValue(JSON value, JSON parameters) const;

    virtual wjson::Value getValue(JSON value, bool semantically_else_syntactically = true) const;
    virtual double getDistance(JSON lhs, JSON rhs) const;
    virtual wjson::Value getPath(JSON lhs, JSON rhs) const;
    virtual double compare(JSON lhs, JSON rhs) const;
    virtual String asString() const;
  };
}

#endif
