#ifndef __symboling_ModalType__
#define __symboling_ModalType__

#include "NumericType.hpp"

namespace symboling {
  /**
   * @class ModalType
   * @description Specifies a level of truth, tau, between -1 (false), 0 (unknown) and 1 (true).
   * @extends Type
   *
   * <a name="parameters"></a>
   * ## Type parameters
   * ### General parameter
   * - `name: ...` The type name. By default the type name is "modal".
   * ### Parameters for the semantic projection
   * - `trinary: ...` If true only the `false` (-1), `unknown` (0), and `true` (1) values are considered, without intermediate value.
   * - `binary: ...` If true only the `false` (-1) and `true` (1) values are considered, without intermediate value.
   * ### Parameters for the distance evaluation
   * - `step: ...` The normalization of the distance `|lhs-rhs|/step` where step is the neighborhood size.
   *   - By default 2.
   * ### Parameters for the geodesic calculation
   * - `atomic: ...`
   *   - If true returns only an atomic path with the two left-hand and right-hand elements. Default is true.
   *   - If false returns a path with all intermediate values, step by step, or precision value by precision value if step is undefined.
   *
   * <a name="specification"></a>
   * ## ModalType specification
   * <table class="specification">
   * <tr style='border: 1px solid black;'><th> Syntactic projection </th><td>
   * - Returns <tt>NAN</tt> if not a parsable number.
   * <br/> - Returns the <tt>zero</tt> if an empty value.
   * <br/> - The value "true" (numerically <tt>1</tt>), "false" (numerically <tt>-1</tt>) and "unknown" or '?' (numerically <tt>0</tt>) are understood.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Semantic projection  </th><td>
   * - If the value is higher than <tt>1</tt> it is set to <tt>true</tt>.
   * <br/> - If the value is lower than <tt>-1</tt> it is set to <tt>false</tt>.
   * <br/> - If <tt>precision</tt> is defined the value is projected to the closest <tt>min + k precision</tt> value, <tt>k</tt> being an integer.
   * <br/> - If in <tt>trinary</tt> mode, values are projected onto the closest <tt>{-1, 0, 1}</tt> value.
   * <br/> &nbsp;&nbsp;&nbsp;&nbsp; <tt>v <- v < -0.5 ? -1 : v > 0.5 ? 1 : 0</tt>
   * <br/> - If in <tt>binary</tt> mode, values are projected onto the closest <tt>{-1, 1}</tt> value.
   * <br/> &nbsp;&nbsp;&nbsp;&nbsp; <tt>v <- v <= 0 ? -1 : 1</tt>
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Distance value       </th><td>
   * The distance is calculated as a weighted value <tt>|lhs-rhs|/step</tt>.
   * <br/> - It is <tt>INFINITY</tt> if not both object are numeric.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Geodesic path        </th><td>
   * The geodesic is assumed to be atomic, i.e. of length 1 if value are equal and 2 if different.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Comparison           </th><td>
   * - Returns either <tt>lhs-rhs</tt> or
   * <br/> <tt>0</tt> if the precision is defined and <tt>|lhs-rhs| < precision</tt>.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Bounds           </th><td>
   * - `[-1, 1]` are the bounds.
   * </td></tr>
   * </table>
   *
   * ## Note:
   * - It is implemented as a `NumericType({ ... min: -1 max: 1 step: 2 atomic: true})`.
   * - This representation is in one to one correspondence with [possibility and necessity modal logic](https://en.wikipedia.org/wiki/Possibility_theory) as implemented using [`getPi()`](#getPi), [`getNu()`](#getNu) and [`getTau()`](#getTau).
   * - This representation is compatible with [2D modal logic](https://hal.inria.fr/hal-03886219) as implemented using [`getTau()`](#getTau) and [`getRho()`](#getRho).
   * - Usual Boolean operations are generalized to modal value as implemented in [`getOp()`](#getOp).
   *
   * @param {JSON|String} parameters The type parameters.
   */
  class ModalType: public NumericType {
    bool trinary = false, binary = false;
protected:
    // Projects any value to the closests numeric value
    virtual double getValueAsDouble(JSON value, bool semantically_else_syntactically = true) const;
public:
    ModalType(JSON parameters);
    ModalType(String parameters);
    ModalType(const char *parameters);
    ModalType();

    /**
     * @function getPi
     * @memberof ModalType
     * @instance
     * @description Returns the possibility value of a modal value.
     * @param {double} tau The modal value.
     * @return The corresponding possibility value, i.e., `min(1, value + 1)`
     */
    static double getPi(double tau);

    /**
     * @function getNu
     * @memberof ModalType
     * @instance
     * @description Returns the necessity value of a modal value.
     * @param {double} tau The modal value.
     * @return The corresponding necessity value, i.e., `max(0, value)`.
     */
    static double getNu(double tau);

    /**
     * @function getTau
     * @memberof ModalType
     * @instance
     * @description Returns the value tau corresponding to a given possibility and necessity.
     * @param {double} pi The possibility value.
     * @param {double} nu The necessity value.
     * @return The corresponding closest modal value, i.e., tau = nu + pi - 1 bounded in [-1, 1].
     */
    static double getTau(double pi, double nu);

    /**
     * @function getRho
     * @memberof ModalType
     * @instance
     * @description Returns the randomness corresponding to a given possibility and necessity.
     * @param {double} pi The possibility value.
     * @param {double} nu The necessity value.
     * @return The corresponding closest randomness value, i.e., rho = nu - pi + 1 bounded in [0, 1].
     */
    static double getRho(double pi, double nu);

    /**
     * @function getOp
     * @memberof ModalType
     * @instance
     * @description Returns the value tau corresponding to a generalized Boolean operation.
     * @param {char} op The binary operation:
     * - `a` : logical `and`, i.e., conjunction.
     * - `o` : logical `or`, i.e., disjunction.
     * - `x` : logical `xor`, i.e., exclusive disjunction.
     * - `m` : logical `minus`, i.e., element set difference.
     * @param {double} tau_1 The possibility value.
     * @param {double} tau_2 The possibility value.
     * @return The corresponding tau value.
     */
    static double getOp(char op, double pi, double nu);
  };
}

#endif
