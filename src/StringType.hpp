#ifndef __symboling_StringType__
#define __symboling_StringType__

#include <math.h>
#include "ListType.hpp"

namespace symboling {
  /**
   * @class StringType
   * @description Specifies a string type.
   * @extends Type
   *
   * <a name="parameters"></a>
   * ## Type parameters
   * ### General parameter
   * - `name: ...` The type name, if specific. By default the type name is `string`.
   * ### Parameters for the syntactic projection
   * - `checkPattern`: The [regex](https://line.gitlabpages.inria.fr/aide-group/aidesys/regex.html) pattern defining if the string is valid. By default any string is valid.
   *   - Using `(value_1|value_2|...)` allows to define an enumeration, i.e., constraints the string to equal only one of these values.
   *     - This corresponds to a [EnumType](./EnumType.html).
   *   - Using a unique value allows to define a constant value, i.e., constraint the string to equal only this value.
   * - `minLength: ...` The minimal length of the string. By default `0`.
   * - `maxLength: ...` The maximal length of the string. By default unbounded (`-1`).
   * ### Parameters for the cost calculation
   * - `deleteCost: ...` The non-negative deleting cost value. Default is `1`.
   * - `insertCost: ...` The non-negative inserting cost value. By default equals to the `deleteCost`.
   * - `editCost: ...` The non-negative editing cost value. Default is 1.
   * ### Parameters for the geodesic calculation
   * - `atomic: ...`
   *   - If true returns only an atomic path with the two left-hand and right-hand elements. Default is true.
   *   - If false returns a path with all intermediate edition.
   * ### Parameters for the bounds calculation
   * - `bounds`: ...` A list of bounds for the string, manually defined.
   * <a name="specification"></a>
   * ## StringType specification
   * <table class="specification">
   * <tr style='border: 1px solid black;'><th> Syntactic projection </th><td>
   * - If the value is not a string but a structured value, a message is issued and the string representation of the structured value is taken.
   * <br/> - If the string syntax is incorrect with respect to the optional <tt>checkPattern</tt> regex, a message is issued, but the string is unchanged.
   * <br/> - If the string length is not in the (optional) <tt>[minLength, maxLength]</tt> bounds, a message is issued, but the string is unchanged.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Semantic projection  </th><td>
   * - Any value as <a href="https://line.gitlabpages.inria.fr/aide-group/wjson/Value.html#asString">wJSON value</a> has a string representation.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Distance value       </th><td>
   * - The distance is computed as a usual <a href="https://en.wikipedia.org/wiki/Edit_distance">edit distance</a>.
   * <br/> &nbsp;&nbsp;&nbsp;&nbsp; - It is delegated to the <a href="./ListType.html">ListType</a> applied on the char list of the string.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Geodesic path        </th><td>
   * - If <tt>atomic</tt> the geodesic is of length 1 if the string are equal, and 2 otherwise.
   * - If not atomic, the geodesic is computed from the edit distance.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Comparison           </th><td>
   * - Returns <tt>0</tt> If each char pair compares equal and both strings have the same length.
   * <br/> - Returns <tt><0</tt> If either the value of the first char that does not match is lower in the left hand side string, or all compared chars match but the left hand side string is shorter.
   * <br/> - Returns <tt>>0</tt> If either the value of the first char that does not match is greater in the left hand side string, or all compared chars match but the left hand side string is longer.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Bounds           </th><td>
   * - The <tt>bounds</tt> are manually entered as parameters, otherwise the empty value is the unique bound.
   * </td></tr>
   * </table>
   *
   * @param {JSON|String} parameters The type parameters.
   */
  class StringType: public ListType {
    std::string checkPattern;
    bool atomic;
    // Converts a string value as a list of char and backward
    static wjson::Value string2chars(JSON value);
    static std::string chars2string(JSON value);
public:
    StringType(JSON parameters);
    StringType(String parameters);
    StringType(const char *parameters);
    StringType();

    virtual wjson::Value getValue(JSON value, bool semantically_else_syntactically = true) const;
    virtual double getDistance(JSON lhs, JSON rhs) const;
    virtual wjson::Value getPath(JSON lhs, JSON rhs) const;
    virtual double compare(JSON lhs, JSON rhs) const;
    virtual JSON getBound(unsigned int index) const;
  };
}

#endif
