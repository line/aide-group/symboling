#ifndef __symboling_SvgType__
#define __symboling_SvgType__

#include <math.h>
#include "ListType.hpp"

namespace symboling {
  /**
   * @class SvgType
   * @description Specifies a symbolic drawing type using a segment of the SVG specification.
   * - This defines the following types:
   *   - `svg_face` which is the root type of a SVG object.
   *   - `svg_:(face|organ|svgTag|attributes|svgD|svgSegment|coordinatesList|segmentType|transformMatrix|style|color|numeric|)` which are the SVG components of a SVG object.
   *   - This thus does NOT define the whole SVG specification.
   * - Conversion from SVG files to JSON files is performed via the Javascript utilities:
   *   - `[.../svg2json.js < file.svg > file.json](./global.html#svg2json)` outputing a `file.json` JSON file.
   *   - `[.../json2svg.js < file.json > file.svg](./global.html#json2svg)` outputing a `file.svg` SVG file.
   * - An example of visual morphing is available on this [demo page](./visualmorphing/titi-toto.html), using a command that
   *   generates a `file1-file2.html` file showing a sequence of intermediate images between `file1` and `file2`:
   *   - `cd .../visualmorphing ; [make](https://gitlab.inria.fr/line/aide-group/symboling/-/blob/master/public/visualmorphing/makefile) [build|rebuild] i1=file1.svg i2=file2.svg`
   * - The utilities [imagemagick convert](https://doc.ubuntu-fr.org/imagemagick) for SVG image conversion and [gifsicle](https://www.lcdf.org/gifsicle) for GIF animation generation are also used at the application level.
   *
   * [!morping titi to toto demo](./visualmorphing/titi-toto.gif)
   *
   * ### Extending the SVG symboling subset
   *
   *  - If a .svg file is not compliant with this SVG segment an unknown type error will be detected. In that case:
   *    - The [SvgType.cpp](https://gitlab.inria.fr/line/aide-group/symboling/-/blob/master/src/SvgType.cpp) is to be upgraded.
   *    - The [json2svg.js](https://gitlab.inria.fr/line/aide-group/symboling/-/blob/master/src/json2svg.js) and [svg2json.js](https://gitlab.inria.fr/line/aide-group/symboling/-/blob/master/src/svg2json.js) may have to be upgraded if new SVG compound types are encountered, i.e. string types corresponding to a record of atomic values.
   *
   *  - Ref: [The mozzila SVG documentation](https://developer.mozilla.org/fr/docs/Web/SVG)
   */
}
#endif
