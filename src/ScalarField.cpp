#include "ScalarField.hpp"
#include "file.hpp"
#include "regex.hpp"
#include <map>

namespace symboling {
  ScalarField::ScalarField(JSON parameters) : parameters(parameters), type(Type::getType(parameters.get("type", "value")))
  {
    // Checks that a type is defined in the parameter data structure
    aidesys::alert(parameters.at("type").isEmpty(), "illegal-argument", "in symboling::ScalarField::ScalarField trajectory parameters without a type '" + parameters.asString() + "'");
    aidesys::alert(parameters.get("type", "") != type.getParameters().get("name", "value"), "illegal-argument", "in symboling::ScalarField::ScalarField trajectory parameters with an undefined type '%s'", parameters.asString().c_str());
    setParameters(parameters);
  }
  ScalarField::ScalarField(String parameters) : ScalarField(wjson::string2json(parameters))
  {}
  ScalarField::ScalarField(const char *parameters) : ScalarField(wjson::string2json(parameters))
  {}
  ScalarField& ScalarField::setParameters(JSON parameters)
  {
    this->parameters.copy(parameters);
    verbose = parameters.get("verbose", verbose);
    epsilon = parameters.get("epsilon", epsilon);
    K_max = parameters.get("K_max", K_max);
    d_max = parameters.get("d_max", d_max);
    nu_max = parameters.get("nu_max", nu_max);
    bounds_count = parameters.get("bounds_count", bounds_count);
    save_interpolation = parameters.get("save_interpolation", save_interpolation);
    if(parameters.isMember("points")) {
      add(parameters.at("points"));
    }
    return *this;
  }
  ScalarField& ScalarField::set(JSON input, double output)
  {
    modified = 0;
    auto it = values.find(input);
    if(it != values.end()) {
      it->second = output;
    } else {
      values[input] = output;
    }
    return *this;
  }
  ScalarField& ScalarField::add(const ScalarField& field)
  {
    modified = 0;
    for(auto it = field.values.begin(); it != field.values.end(); it++) {
      values.insert(*it);
    }
    return *this;
  }
  ScalarField& ScalarField::add(JSON values)
  {
    if(values.isArray()) {
      for(unsigned int l = 0; l < values.length(); l++) {
        add(values.at(l));
      }
    } else {
      if(values.isMember("input")) {
        set(values.at("input"), values.get("output", NAN));
      } else if(!values.isEmpty()) {
        set(values, NAN);
      }
    }
    return *this;
  }
  ScalarField& ScalarField::add(String values)
  {
    return add(wjson::string2json(values));
  }
  ScalarField& ScalarField::add(const char *values)
  {
    return add(wjson::string2json(values));
  }
  ScalarField& ScalarField::load(String filename)
  {
    return add(wjson::string2json(aidesys::load(filename)));
  }
  void ScalarField::save(String filename, bool pretty, bool strict) const
  {
    aidesys::save(filename, getValues().asString(pretty, strict));
  }
  ScalarField& ScalarField::erase(const JSON input)
  {
    values.erase(input);
    return *this;
  }
  ScalarField& ScalarField::clear()
  {
    modified = 0;
    values.clear();
    return *this;
  }
  JSON ScalarField::getValues() const
  {
    static wjson::Value result;
    result.clear();
    for(auto it = values.cbegin(); it != values.cend(); it++) {
      wjson::Value item;
      item["input"] = it->first, item["output"] = it->second;
      result.add(item);
    }
    return result;
  }
  double ScalarField::getValue(JSON input, unsigned int K_max, double d_max, bool save_interpolation)
  {
    K_max = K_max == (unsigned int) -1 ? this->K_max : K_max, d_max = std::isnan(d_max) ? this->d_max : d_max, save_interpolation |= save_interpolation;
    auto it = values.find(input);
    if(it != values.end() && !std::isnan(it->second)) {
      aidesys::alert(verbose, "", "ScalarField::getValue('%s') = %f, known", input.asString().c_str(), it->second);
      return it->second;
    }
    if(K_max == 0) {
      aidesys::alert(verbose, "", "ScalarField::getValue('%s') = NAN, %s", input.asString().c_str(), it != values.end() ? "unknown" : "undefined");
      return NAN;
    }
    std::multimap < double, std::pair < wjson::Value, double >> neighborhood;
    // Constructs the neighborhood ordered map
    unsigned int count = 0;
    for(auto it = values.begin(); it != values.end(); it++) {
      if(!std::isnan(it->second)) {
        count++;
        double d = getDistance(input, it->first);
        if(d < d_max) {
          neighborhood.insert(std::pair { d, *it });
          if(K_max != (unsigned int) -1 && neighborhood.size() > K_max) {
            neighborhood.erase(std::prev(neighborhood.end()));
          }
        }
      }
    }
    aidesys::alert(verbose, "", "ScalarField::getValue('%s', K_max: %d d_max: %f < diameter: %f save: %d size: %d >= count: %d", input.asString().c_str(), K_max, d_max == DBL_MAX ? 0 : d_max, getDiameter(), save_interpolation, (int) neighborhood.size(), count);
    if(neighborhood.size() == 0) {
      // Fall back value if the neighborhood is empty
      return count > 0 ? getValue(input, 1, DBL_MAX, false) : 0;
    } else {
      // Iteratively reduces the neighborhood by interpolation
      while(neighborhood.size() > 1) {
        // Searchs two points in the neighborhood at the smallest relative distance
        auto it1_min = neighborhood.cend(), it2_min = neighborhood.cend();
        double d_min12 = DBL_MAX;
        for(auto it1 = neighborhood.cbegin(); it1 != neighborhood.cend(); it1++) {
          for(auto it2 = neighborhood.cbegin(); it2 != it1; it2++) {
            double d = getDistance(it1->second.first, it2->second.first);
            if(d < d_min12) {
              it1_min = it1, it2_min = it2, d_min12 = d;
            }
          }
        }
        aidesys::alert(it1_min == neighborhood.cend() || it2_min == neighborhood.cend(), "illegal-state", "in ScalarField::getValue unable to identify two distinct values (%d, %d) while sixe = %d > 1", it1_min == neighborhood.cend(), it2_min == neighborhood.cend(), neighborhood.size());
        // Searches on the path between those two points the point with the input smallest relative distance
        double d_min = it1_min->first;
        unsigned int i_min = 0;
        wjson::Value path = type.getPath(it1_min->second.first, it2_min->second.first);
        for(unsigned int i = 1; i < path.length() - 1; i++) {
          double d = getDistance(input, path[i]);
          if(d < d_min) {
            d_min = d, i_min = i;
          }
        }
        // Interpolates the value at this closest point
        double
          w1 = getDistance(it1_min->second.first, path[i_min]),
          w2 = getDistance(it2_min->second.first, path[i_min]),
          w = w1 + w2,
          v = w > 0 ? (w2 * it1_min->second.second + w1 * it2_min->second.second) / w : it1_min->second.second;
        aidesys::alert(verbose, "", "\t ('%s' -> %f, '%s' -> %f) => '%s' -> %f, d_min12 = %f d_min = %f #%d", it1_min->second.first.asString().c_str(), it1_min->second.second, it2_min->second.first.asString().c_str(), it2_min->second.second, path[i_min].asString().c_str(), v, d_min12, d_min, (int) neighborhood.size());
        // Replaces the formers by the latter
        neighborhood.erase(it2_min);
        neighborhood.erase(it1_min);
        neighborhood.insert(std::pair { d_min, std::pair { path[i_min], v } });
      }
      double result = neighborhood.cbegin()->second.second;
      if(save_interpolation) {
        set(input, result);
      }
      return result;
    }
  }
  wjson::Value ScalarField::getExtrapolations(JSON input, JSON reference, double d_max, double nu_max, unsigned int bounds_count)
  {
    d_max = d_max == std::isnan(d_max) ? this->d_max : d_max, nu_max = nu_max == std::isnan(nu_max) ? this->nu_max : nu_max, bounds_count = bounds_count == (unsigned int) -1 ? this->bounds_count : bounds_count;
    JSON bounds = type.getBounds(bounds_count);
    wjson::Value results;
    double d_0 = getDistance(input, reference);
    for(unsigned int j = 0; j < bounds.length(); j++) {
      JSON bound = bounds.at(j);
      wjson::Value path = type.getPath(input, bound);
      if(verbose) {
        if(j == 0) {
          printf("getExtrapolations('%s', '%s', d_max: %f, nu_max: %e count: %d)\n", input.asString().c_str(), reference.asString().c_str(), d_max, nu_max, bounds_count);
        }
        printf("  bound: '%s' => ", bound.asString().c_str());
        for(unsigned int i = 1; i < path.length(); i++) {
          JSON result_i = path.at(i);
          double d_i = getDistance(result_i, input);
          if(d_max < d_i) {
            break;
          }
          double d_j = getDistance(result_i, reference);
          if(d_0 < d_j && d_i + d_0 - d_j < nu_max) {
            printf("'%s': %f ", result_i.asString().c_str(), d_i);
          }
        }
        printf("\n");
      }
      for(unsigned int i = 1; i < path.length(); i++) {
        JSON result_i = path.at(i);
        double d_i = getDistance(result_i, input);
        if(d_max < d_i) {
          break;
        }
        double d_j = getDistance(result_i, reference);
        if(d_0 < d_j && d_i + d_0 - d_j < nu_max) {
          results.add(result_i);
        }
      }
    }
    return results;
  }
  wjson::Value ScalarField::getBarycenter() const
  {
    if((modified & 0x1) == 0 || barycenter.isEmpty()) {
      modified |= 0x1;
      std::unordered_map < wjson::Value, double > values = this->values;
      // Checks and normalizes the values
      {
        double s = 0;
        for(auto it = values.cbegin(); it != values.cend(); it++) {
          if(std::isnan(it->second) || it->second <= epsilon) {
            aidesys::alert(it->second < 0, " illegal-argument", "in symboling::ScalarField::getBarycenter negative weighted are not allowed, whereas w(" + it->first.asString() + ") = %g, the value is skipped", it->second);
            values.erase(it);
          } else {
            s += it->second;
          }
        }
        for(auto it = values.begin(); it != values.end(); it++) {
          it->second /= s;
        }
      }
      if(values.size() == 0) {
        return wjson::Value::EMPTY;
      }
      // Iteratively reduces the values
      while(values.size() > 1) {
        if(verbose) {
          printf("ScalarField::getBarycenter: #%ld\n", values.size());
          for(auto it = values.cbegin(); it != values.cend(); it++) {
            printf("\t '%s': %f\n", it->first.asString().c_str(), it->second);
          }
        }
        // Searchs two points in the values at the smallest relative distance
        auto it1_min = values.cbegin(), it2_min = it1_min;
        double d_min12 = DBL_MAX;
        for(auto it1 = values.cbegin(); it1 != values.cend(); it1++) {
          for(auto it2 = values.cbegin(); it2 != it1; it2++) {
            double d = getDistance(it1->first, it2->first);
            if(d < d_min12) {
              it1_min = it1, it2_min = it2, d_min12 = d;
            }
          }
        }
        double w = it1_min->second + it2_min->second, d_opt = d_min12 * it2_min->second / w, d_1 = 0;
        // Finds a point on the path at a distance corresponding to the weighting
        unsigned int i_min = 0;
        wjson::Value path = type.getPath(it1_min->first, it2_min->first);
        for(unsigned int i = 0; i < path.length(); i++) {
          double d_0 = getDistance(it1_min->first, path.at(i));
          if(d_1 < d_opt && d_opt <= d_0) {
            i_min = d_0 - d_opt < d_opt - d_1 ? i : i > 0 ? i - 1 : i;
            break;
          }
          d_1 = d_0;
        }
        // Replaces the formers by the latter
        values.erase(it1_min);
        values.erase(it2_min);
        values.insert(std::pair { path.at(i_min), w });
      }
      barycenter = values.cbegin()->first;
      if(verbose) {
        printf("ScalarField::getBarycenter: #%ld\n", values.size());
        for(auto it = values.cbegin(); it != values.cend(); it++) {
          printf("\t '%s': %f\n", it->first.asString().c_str(), it->second);
        }
      }
    }
    return barycenter;
  }
  double ScalarField::getDiameter() const
  {
    if((modified & 0x2) == 0 || diameter < 0) {
      modified |= 0x2;
      diameter = 0;
      for(auto it1 = values.cbegin(); it1 != values.cend(); it1++) {
        for(auto it2 = values.cbegin(); it2 != it1; it2++) {
          double d = getDistance(it1->first, it2->first);
          if(diameter < d) {
            diameter = d;
          }
        }
      }
    }
    return diameter;
  }
  double ScalarField::getDistance(JSON lhs, JSON rhs) const
  {
    // Implements a cache mechanism
    auto itl = distances.find(lhs);
    if(itl != distances.end()) {
      auto d_lhs = distances.at(lhs);
      auto itr = d_lhs.find(rhs);
      if(itr != d_lhs.end()) {
        return d_lhs.at(rhs);
      }
    }
    return distances[lhs][rhs] = distances[rhs][lhs] = type.getDistance(lhs, rhs);
  }
}
