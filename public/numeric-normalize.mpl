# Data normalization

## Linear scaling

u_0 := x -> (2 * x - (min + max)) / (max - min):
ok = simplify({ u_0(min) = -1, u_0(max) = 1});

## Quadratic biasing

u_np1 := u_n -> u_n - z_n / (1 - z_n^2) * (1 - u_n^2):
ok = simplify({ u_np1(-1) = -1, u_np1(1) = 1, u_np1(z_n) = 0 });

### Showing that z_n = 0 yields the identity function
u_np1_0  := unapply(simplify(subs(z_n = 0, u_np1(u_n))), u_n);

### Studying the cases where z_n = +/- z_max
z_max := sqrt(2) - 1:
u_np1_0p := unapply(simplify(subs(z_n =  z_max, u_np1(u_n))), u_n);
u_np1_0n := unapply(simplify(subs(z_n = -z_max, u_np1(u_n))), u_n);
plot([u_np1_0p(u), min(1, max(-1, (u-z_max)/(1-z_max)))], u = -1..1);
plot([u_np1_0n(u),  min(1, max(-1, (u+z_max)/(1-z_max)))], u = -1..1);
#### It is a strictly monotonic quadratic profile
#### It is flat only at the interval bound
#### If z_n > z_max then u_np1_0p(z_n) > 0 and u_np1_0p(z_n) <  z_n 
#### If z_n < z_max then u_np1_0n(z_n) < 0 and u_np1_0n(z_n) >  z_n
#### With an almost linear profile of slope 1 / (1 - z_max) = 1.7

### Numeric evaluation of the transformation degree
u_degree := proc(z)
 local n, z0 := evalf(signum(0, z, 0) * (sqrt(2) - 1)), z1 := z:
 if z <= -1 or z >= 1 then 0 elif z = 0 then 1 
 else
   for n while abs(z1) > abs(z0) do z1 := z1 - z0 / (1 - z0 * z0) * (1 - z1 * z1) od:
   return 2 * n
 fi
end:
l := []: for z from -1 to 1 by 0.01 do l := [op(l), [z, u_degree(z)]] od:
#plot(l);
##### At 1% of a bound the degree is 16 for 8 iterations
l[2];
 






