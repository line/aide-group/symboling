%
% This file is used in  
%   https://gitlab.inria.fr/line/aide-group/aide/-/blob/master/etc/tex_to_pdf.sh
% and command are documented in http://aide-line.inria.fr/build/www/etc.html#.tex_to_pdf
%

% Defines a minimal layout for pdf display

\documentclass[a4paper,12pt,landscape,pdftoolbar=false,pdfmenubar=false]{article}
\pagestyle{empty} 
\topmargin 0cm \oddsidemargin 0cm \evensidemargin 0cm 
\setlength{\parindent}{0in} 
\setlength{\parskip}{3mm} 
\usepackage[margin=2cm]{geometry}

% Here are the used packages

\usepackage[utf8]{inputenc}
\DeclareUnicodeCharacter{00B0}{\textsuperscript{o}}

\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{amsmath}\usepackage{amssymb}\usepackage{amsfonts}
\usepackage{array}
\usepackage{times}
\usepackage{color}
%\usepackage{algpseudocode}\usepackage{algorithm}
\usepackage{listings}

% Here is tha macro for conditional compilation

\def\aidebuild {}

% Here are some used commands

\newcommand{\deq}{\stackrel {\rm def}{=}} 
\newcommand{\eqline}[1]{~\vspace{0.3cm}\\\centerline{$#1$}\vspace{0.3cm}\\}
\newcommand{\tab}{\hphantom{6mm}}
\newcommand{\hhref}[1]{\href{#1}{#1}}

% Here are some homemade commands to define minimal slides

\newcommand{\slide}[1]{\clearpage\fbox{\parbox[t][16.5cm][t]{\textwidth}{\Huge #1}}\newpage}
\newcommand{\stitle}[1]{~\vspace{1cm}\\\centerline{\fontsize{40}{50}\selectfont \bf #1}\vspace{0.5cm}\\}
\newcommand{\sright}[1]{\begin{flushright}#1\tab\end{flushright}}
\newcommand{\scenter}[1]{\begin{center}#1\end{center}}
\newcommand{\stwo}[4]{\begin{tabular}{ll}\parbox{#1\textwidth}{#3}&\parbox{#2\textwidth}{#4}\end{tabular}}

\begin{document}

\vspace{4cm} \centerline{\Huge Scalar field on a symbolic data structure}

\subsubsection*{Position of the problem: Context and basic mechanism}

We consider manipulating symbolic data structure within usual machine learning algorithms, as detailed
here\footnote{\url{https://line.gitlabpages.inria.fr/aide-group/symboling/symboling.pdf}}. Such data structure can be used in problem solving defining a trajectory from an initial state to a final goal as developed here\footnote{\url{https://line.gitlabpages.inria.fr/aide-group/symboling/trajectory.pdf}} or for reinforcement symbolic learning as developed
there\footnote{\url{https://line.gitlabpages.inria.fr/aide-group/symboling/qlearning.pdf}}. In both cases a scalar-field (i.e. a function that associates a real number to a given data structure), namely a potential in the former case and a value function in the latter case, has to be defined. Such a data structure is introduced and presented here.

Given a symbolic data structure ${\bf s} \in {\cal S}$, where ${\cal S}$ stands for a set of data structures, finite but huge and thus intractable to enumerate, a scalar field $f$ is a real-valued function:
\eqline{\begin{array}{lrcl}f:& {\cal S} &\hookrightarrow& {\cal R} \\ & {\bf s} &\rightarrow r \\ \end{array}}
while, in our context, we define the function by setting some values:
\eqline{f({\bf s}_1) \leftarrow r_1, f({\bf s}_2) \leftarrow r_2, \cdots}
and would like to infer values for data structures in a neighborhood of these predefined values.

\subsubsection*{Position of the problem: Metric space with geodesic}

We are in a metric space, thanks to the editing distance, and this space is also equipped with geodesic between two data structures. This path between two data structures ${\bf s}_1$ and ${\bf s}_2$ is a discrete sequence built of the all intermediate data-structures corresponding a truncated sequence of editing operations of minimal cost to transform a data structure to another. The key point is that for any data structure ${\bf s}_k$ on this path, by construction of the editing distance\footnote{Let us consider between ${\bf s}_1$ and ${\bf s}_2$ an intermediate data structure ${\bf s}_k$ defined by a sub-sequence of editing operations that has been used to calculate $d({\bf s}_1, {\bf s}_2)$. By construction, the cost $c({\bf s}_1, {\bf s}_k)$ of the sub-sequence of operation from ${\bf s}_1$ to ${\bf s}_k$ plus the cost $c({\bf s}_k, {\bf s}_2)$ of the sub-sequence of operation from ${\bf s}_k$ to ${\bf s}_2$ is equal to $d({\bf s}_1, {\bf s}_2)$. \\
The cost $c({\bf s}_1, {\bf s}_k)$ can not be lower that the distance $d({\bf s}_1, {\bf s}_k)$, because $d({\bf s}_1, {\bf s}_k)$ corresponds to the cost of the lower sequence of operation from ${\bf s}_1$ to ${\bf s}_k$, by definition. \\
Since it is also the case from ${\bf s}_k$ to ${\bf s}_2$, we also have $c({\bf s}_k, {\bf s}_2) \geq d({\bf s}_k, {\bf s}_2)$.\\
Then:
\eqline{\left\{\begin{array}{l}
c({\bf s}_1, {\bf s}_k) + c({\bf s}_k, {\bf s}_2) = d({\bf s}_1, {\bf s}_2) \\
c({\bf s}_1, {\bf s}_k) \geq d({\bf s}_1, {\bf s}_k) \\
c({\bf s}_k, {\bf s}_2) \geq d({\bf s}_k, {\bf s}_2) \\ \end{array}\right.
\Rightarrow d({\bf s}_1, {\bf s}_2) \geq d({\bf s}_1, {\bf s}_k) + d({\bf s}_k, {\bf s}_2),}
while from the triangular inequality we also have $d({\bf s}_1, {\bf s}_2) \leq d({\bf s}_1, {\bf s}_k) + d({\bf s}_k, {\bf s}_2)$, so that we only must conclude about the equality.\\
Another argument is that this cost $c({\bf s}_1, {\bf s}_k)$ can not be higher that the distance $d({\bf s}_1, {\bf s}_k)$, otherwise it means that there is a ``better'' sub-sequence of operation from ${\bf s}_1$ to ${\bf s}_k$, which, without changing the sub-sequence of operation from ${\bf s}_k$ to ${\bf s}_2$, will lower $d({\bf s}_1, {\bf s}_2)$, which is a contradiction. We thus must have $c({\bf s}_1, {\bf s}_k) = d({\bf s}_1, {\bf s}_k)$, with the same result for $c({\bf s}_k, {\bf s}_2)$, so that we rederive the equality again.}:
\eqline{d({\bf s}_1, {\bf s}_k) + d({\bf s}_k, {\bf s}_2) = d({\bf s}_1, {\bf s}_2),}
which is the reason we call it a geodesic.

A key point is that the path is not unique: in our case, at the implementation level we have made the following choices:
\\ \tab - For lists, i.e., sequences, at equal cost we prefer edition than deletion, and deletion than insertion, i.e., we to maintain, else reduce the list length than increase it.
\\ \tab - For sets, i.e., unordered list, we first consider association of maximal cost before association of lower costs.
\\ \tab - For records, i.e. labeled tuples, we consider the transformation in the item order, i.e. modifying the 1st item first. This is coherent with the fact that at the semantic level, the item order is taken into account.
\\ Such choices seem reasonable but are somehow arbitrary, and the semantic level, i.e., when a meaning is given the different elements this have an impact on the result. Interesting enough, this is easily adaptable.

It is easy to verify, that for each data structure on such a geodesic, we have the minimal distance to both extremities, and the sub-geodesic segment corresponds to a geodesic of minimal length between the intermediate data structure and each extremity.

We also easily verify that our design choice of minimal geodesic is stable, in the sense that the geodesic segment corresponds in the three cases (list, set and record) to a geodesic that comply with the corresponding uniqueness requirement.

A more efficient, but exponentially costly design choice, would have been to generate all possible paths of minimal distance between two data structures, implementing such a mechanism is easy but rapidly intractable when the data structure size increases.

\subsubsection*{Implementation: Interpolation mechanism in a neighborhood} \label{interpolation}

Given a data structure ${\bf s}_0$ for which the scalar value has not been fixed, we need to interpolate this value given known values in a neighborhood.

Given the fact we only have a metric space, i.e. distances, we define such a neighborhood which two parameters:
\\ - Its cardinality $K$, i.e., we consider at most the $K$ closest data structure.
\\ - A maximal distance $d_{\max}$, i.e., we consider data structures which distance is lower than $d_{\max}$.
\\ which is a very standard way of defining neighborhoods, for instance when considering operations on manifolds (see, e.g.,  \cite{burges_dimension_2010}) or graph manipulation (see, e.g. \cite{pandove_local_2017}).

Given such a neighborhood, it seems that, given the available tools we are left with the following choices:
\\ - If $K = 0$ the value is undefined.
\\ - If $K = 1$ we only can approximate the data structure scalar value by considering the closest known predefined value, i.e., the value of data structure which is in this neighborhood singleton.
\\ \begin{tabular}{cc}
\parbox{0.4\textwidth}{\vspace{-5cm} Then, if $K > 1$, we propose to iteratively reduce the neighborhood to a singleton, i.e., until $K=1$. We consider, in the initial neighborhood, the two data structures which relative distance is minimal and replace the two data structures by a data structure on the their geodesic which is as close as possible to ${\bf s}_0$, thus reducing the neighborhood size by 1.
\\As graphically represented here, ${\bf s}_1$ and ${\bf s}_2$ are replaced by the value on their geodesic as closed as possible to ${\bf s}_0$, which is then related to ${\bf s}_3$ by a geodesic, making the same choice again, which finally is related to ${\bf s}_4$, yielding to a close approximation to the unknown ${\bf s}_0$ value.}&
\includegraphics[width=0.5\textwidth]{./scalarfield-interpolation.png} \\
\end{tabular} \\
Given this mechanism for two data structures ${\bf s}_i$ and ${\bf s}_j$ and choosing as best location for the approximation, the data structure ${\bf s}_\bullet$ on the geodesic which distance to ${\bf s}_0$ is minimal, we can interpolate the corresponding scalar value $r_\bullet$ by a linear interpolation\footnote{This value is well defined as soon as:
\eqline{{\bf s}_i \neq {\bf s}_j \Rightarrow 0 < d({\bf s}_i, {\bf s}_j) < d({\bf s}_i, {\bf s}_\bullet) + d({\bf s}_j, {\bf s}_\bullet),} the denominator being higher than 0.}
\eqline{r_\bullet \leftarrow \frac{d({\bf s}_i, {\bf s}_\bullet) \, r_j + d({\bf s}_j, {\bf s}_\bullet) \, r_i}{d({\bf s}_i, {\bf s}_\bullet) + d({\bf s}_j, {\bf s}_\bullet)}.}

Choosing to merge the closest data structures and replace them by the intermediate data structure which is as closed as possible to the data structure value to approximate is a reasonable choice, in this context, and yields to a linear algorithm in terms of complexity\footnote{At this stage, our method is sub-optimal since we only consider one geodesic, and not all possibles geodesics, and at the cost of a combinatory explosion, we could also have considered all possible geodesics between all pairs of data structures and all possible sub-geodesics between the data structures on each geodesic, to attempt to obtain a data structure as closed as possible to ${\bf s}_0$, but this would not have been a tractable design choice.}.

Although we do not need to explicitly consider that the data structures live in an abstract manifold, i.e., a space that is locally Euclidean up to the first order, we in fact propose a method inspired by such formalism.

This method is to be compared with a straightforward linear approximation:
\eqline{r_0 \leftarrow \frac{\sum_{i=1}^K \nu(d({\bf s}_0, {\bf s}_i)) \, r_i}{\sum_{i=1}^K \nu(d({\bf s}_0, {\bf s}_i))}}
where $\nu(d)$ is some decreasing function of the distance\footnote{For instance:
\eqline{\nu(d) \deq e^{-\frac{d}{d_{\max}}} \simeq 1 - \frac{d}{d_{\max}}.}} The advantage of the former method is that it is more local, i.e., we perform linear approximation only between pair of data structures which are as close as possible to each-other, while this latter straightforward formula interpolates values that are more distant.

Another interesting point is that the same iterative algorithm can be adapted to approximate a barycenter between data structured.

\subsubsection*{Implementation: Computing the barycenter of the input values} \label{barycenter}

Given a set or subset of weighted data structures $\{({\bf s}_1, w_1), ({\bf s}_2, w_2), \cdots \}$ we would like to approximate a data structure ${\bf s}_0$, such that\footnote{
In the Euclidean case, $d({\bf v}_0, {\bf v}_j) \deq \|{\bf v}_0 - {\bf v}_j\|$ and with $e=2$, we easily obtain for this
convex quadratic criterion by derivation of the normal equations:
\eqline{{\bf v}_0 \deq \frac{\sum_{i=1}^K w_i \, {\bf v}_i}{\sum_{i=1}^K w_i},}
obtaining the usual formula of a barycenter, or weighted centroids (see e.g. \url{https://en.wikipedia.org/wiki/Centroid}), since linear combination of vectors ${\bf v}_i$ is defined. However, this is not the case with data structures, where we only can consider distances.}:
\eqline{{\bf s}_0 \simeq \mbox{arg min} \; l, l \deq \sum_{i=1}^K w_i \, d({\bf s}_0, {\bf s}_j)^e, e > 0, \forall i, w_i \geq 0}
which corresponds to a generalization of a barycenter, as it is required for instance in clustering algorithms\footnote{E.g. \url{https://en.wikipedia.org/wiki/K-means_clustering}}.

If the weights $\{\cdots w_i \cdots\}$ corresponds the value $\{\cdots r_i \cdots\}$ we obtain the weighted centroids of the predefined set values.

If $K=1$, obviously, ${\bf s}_0 = {\bf s}_1$ is the solution since we have a minimum when $d({\bf s}_0, {\bf s}_1) = 0$.

If $K=2$, it is a reasonable choice to minimize $l = w_1 \, d({\bf s}_0, {\bf s}_1)^e + w_2 \, d({\bf s}_0, {\bf s}_2)^e$ on the geodesic between ${\bf s}_1$ and ${\bf s}_2$, because $d({\bf s}_0, {\bf s}_1) + d({\bf s}_0, {\bf s}_2) = d({\bf s}_1, {\bf s}_2)$ is minimal\footnote{Let us consider a general data structure ${\bf s}$, writing $d_1 \deq d({\bf s}, {\bf s}_1)$, $d_2 \deq d({\bf s}, {\bf s}_2)$,  $d_{12} = d({\bf s}_1, {\bf s}_2)$, thus $d_1 + d_2 \geq d_{21}$, i.e., $d_1 + d_2 = d_{12} + \epsilon, \epsilon \geq 0$, we are now left to minimize $l = w_1 \, d_1^e + w_2 \, (d_{12} + \epsilon - d_1)^e$ and it is clear that this is minimal for $\epsilon = 0$.}. This is easily performed by scanning the different data structures to find the minimal value, when not simpler\footnote{We left to the reader to verify that, if $e \leq 1$ the minimum is on one geodesic extremity, while if $e > 1$ there is a unique minimum between both extremities, while for $e = 2$, we obtain explicitly: \eqline{w_2 \, d({\bf s}_0, {\bf s}_1) = w_1 \, d({\bf s}_0, {\bf s}_2).}}.

For $K > 2$, as before we can only work on geodesics and we propose to consider the two data structures ${\bf s}_i$ and ${\bf s}_j$ with smallest distance, calculate on their geodesic the barycenter ${\bf s}_\bullet$ with weight $w_\bullet = w_i + w_j$,
replacing the two former data structures by the latter, thus decreasing the set of data structure cardinal by 1, this being iterated until $K = 2$. This is not an optimal strategy, since we only consider a linear number of geodesics and not all possible ones, but this provides an approximate estimation, by considering only local operations.

\subsubsection*{Implementation: Computing extrapolation away from a neighborhood value.} \label{extrapolation}

Another issue is to extrapolate the path from a value with respect to another one: Given a value ${\bf s}_0$, instead of finding a path towards a reference value ${\bf s}_{\sharp}$, we aim at create a path ``away'' from this value. This is to be used to avoid an obstacle or a forbidden state when solving a problem, as discussed
here\footnote{\url{https://line.gitlabpages.inria.fr/aide-group/symboling/trajectory.pdf}} or to explore new value when learning a behavior as quoted there\footnote{\url{https://line.gitlabpages.inria.fr/aide-group/symboling/qlearning.pdf}}.

The caveat is that we do not have a-priory such a mechanism\footnote{\label{extraword} Given the standard Levenshtein editing distance\footnote{\url{https://en.wikipedia.org/wiki/Levenshtein_distance}.} the editing operation have no well-defined ``inverse''. Let us consider a trivial example between {\tt mama} and {\tt mamie} easily obtained via, e.g., {\tt mama} $\rightarrow$ {\tt mame} $\rightarrow$ {\tt mamie} in two steps. Defining a string edition {\tt mama} that is ``away from'' {\tt mamie} is totally ill-defined, we could insert, delete or change any letter. Furthermore, considering the trivial example of words, there is a very little chance that when inserting, deleting or changing any letter we draw a syntactically correct word, and even less chance to generate a semantically useful word.}. We thus have to introduce some arbitrary exploration at this stage, which is not a caveat but an interesting feature to explore ``creative'' solutions (in the wide sense). Furthermore, as explified in footnote${}^{\ref{extraword}}$, when trying to simply generate unconstrained random values around a current value, by some simple random draw, given complex symbolic data structure there is almost no chance to generate a useful or even meaningful random value. We will get around with the idea of choosing some alternative ``escape'' target ${\bf s}_i$ in order to find on the path towards these targets a data structure away from the reference. 

\begin{tabular}{cc}
\parbox{0.4\textwidth}{\vspace{-5cm}
Given a current data structure ${\bf s}_0$ and a reference data structure ${\bf s}_{\sharp}$ the objective is to extrapolate a path beyond the geodesic between the two, as illustrated here. The key idea is to explore geodesics between ${\bf s}_0$ and other remote ``escape'' data structures ${\bf s}_i$.
\\ Following this track, we have two main options: search an escape data structure among the scalar field predefined data structures, or generate a new escape data structure. We now consider the first option and will discuss the second option in the next subsection.
\\Beyond, choosing the best extrapolation path is an under-determined problem. Let us discuss how to better specify it.
} &
\includegraphics[width=0.5\textwidth]{./scalarfield-extrapolation.png} \\
\end{tabular}

Obviously the problem is meaningful only if $d({\bf s}_{0}, {\bf s}_{\sharp}) > 0$; otherwise any value different from ${\bf s}_{\sharp}$ is a kind of extrapolation.

The most interesting criterion is that a data structure ${\bf s}_{\dagger}$ is on a geodesic away from ${\bf s}_{\sharp}$, if a geodesic from ${\bf s}_{\dagger}$ to ${\bf s}_{\sharp}$ includes ${\bf s}_0$. This means, in term of distances:
\eqline{\nu({\bf s}_{\dagger}) \deq d({\bf s}_{\dagger}, {\bf s}_{0}) + d({\bf s}_{0}, {\bf s}_{\sharp}) - d({\bf s}_{\dagger}, {\bf s}_{\sharp}) = 0}
as discussed previously, while $\nu({\bf s}_{\dagger}) \geq 0$ in the general case, due to the distance triangular inequality. We thus have some advantage to minimize $\nu({\bf s}_{\dagger})$, i.e., find a data structure on a geodesic from ${\bf s}_0$ to ${\bf s}_i$ which minimize $\nu({\bf s}_{\dagger})$.

The second aspect is that we want the extrapolation to be as ``away'' as possible, i.e. that $d({\bf s}_{0}, {\bf s}_{\sharp})$ must be as high as possible, if it allows to define a prolongation of the ${\bf s}_0$ to ${\bf  s}_{\sharp}$ geodesic, whereas these are only valid locally. We thus have to bound $d({\bf s}_{0}, {\bf s}_{\sharp}) \leq d_{\mbox{local}}$ in order to remain in a local neighborhood.

Given this constraint and specification we now have a well-defined algorithmic definition: {\em given all escape data structures, find on each related geodesic the data structure of maximal distance, but below $d_{\mbox{local}}$, that minimizes $\nu({\bf s}_{\dagger}) \leq 0$}.

\paragraph{Complementary specification of the extrapolation.}~\\

A step further, to attempt to better specify what ``away'' could mean, and for a data structure ${\bf s}_{\dagger}$ on the geodesic between ${\bf s}_0$ and ${\bf s}_i$ we propose two criteria,
\\ One on the distance, selecting only target ${\bf s}_{\dagger}$ with\footnote{In the Euclidean case, for any point ${\bf v}_k \deq \alpha \, {\bf v}_0 + (1 - \alpha) \, {\bf v}_{i}, \alpha \in [0, 1]$ on the geodesic between ${\bf v}_0$ and ${\bf v}_{\dagger}$ which a rectilinear segment, if $d({\bf v}_{\dagger}, {\bf v}_{\sharp}) < d({\bf v}_0, {\bf v}_{\sharp})$ we easily obtain:
\eqline{d({\bf v}_k, {\bf v}_{\sharp}) = \alpha \, d({\bf v}_0, {\bf v}_{\sharp}) + (1 - \alpha) \, d({\bf v}_{i}, {\bf v}_{\sharp}) < \alpha \, d({\bf v}_0, {\bf v}_{\sharp}) + (1 - \alpha) \, d({\bf v}_{0}, {\bf v}_{\sharp}) = d({\bf v}_0, {\bf v}_{\sharp}),}
thus we cannot obtain a extrapolation with $d({\bf v}_k, {\bf v}_{\sharp}) > d({\bf v}_{0}, {\bf v}_{\sharp})$ and we easily verify that on the reverse if $d({\bf v}_{\dagger}, {\bf v}_{\sharp}) > d({\bf v}_0, {\bf v}_{\sharp})$ any $\alpha < 1$ allows to find an extrapolation. The assumption is that for ${\bf v}_k$ close to ${\bf v}_0$ the data structure metric space is regular enough for this property to be verified.}:
\eqline{d({\bf s}_{\dagger}, {\bf s}_{\sharp}) > d({\bf s}_{\dagger}, {\bf s}_0),}
i.e., closer to the current data structure than the reference data structure.
\\ One on a generalization of the orientation, selecting only target ${\bf s}_{\dagger}$ with\footnote{From the triangle cosine law:
\eqline{d({\bf v}_{\dagger}, {\bf v}_{\sharp})^2 = d({\bf v}_{0}, {\bf v}_{\sharp})^2 + d({\bf v}_{\dagger}, {\bf v}_{0})^2 - 2 \, d({\bf v}_{0}, {\bf v}_{\sharp}) \, d({\bf v}_{\dagger}, {\bf v}_{0}) \, \cos\left(\gamma\right), \gamma \deq \widehat{\overrightarrow{{\bf v}_{0} {\bf v}_{\sharp}}, \overrightarrow{{\bf v}_{0} {\bf v}_{\dagger}}},}
and $d({\bf v}_{\dagger}, {\bf v}_{\sharp})$ is minimal, if $\gamma = \pi$, while obvious geometric properties of the triangle show that:
\eqline{d({\bf v}_{\dagger}, {\bf v}_{\sharp}) > d({\bf v}_0, {\bf v}_{\sharp}) \Leftrightarrow \gamma > \frac{\pi}{2} \Leftrightarrow \cos(\gamma) < 0}
which can be written only in terms of distance, in the case where $d({\bf v}_{0}, {\bf v}_{\sharp})$ and $d({\bf v}_{\dagger}, {\bf v}_{0})$ do not vanish, as:
\eqline{d({\bf v}_{\dagger}, {\bf v}_{0})^2 + d({\bf v}_{0}, {\bf v}_{\sharp})^2 < d({\bf v}_{\dagger}, {\bf v}_{\sharp})^2,}
while we still consider that this property is locally valid beyond the Euclidean case.}:
\eqline{d({\bf s}_{\dagger}, {\bf s}_{0})^2 + d({\bf s}_{0}, {\bf s}_{\sharp})^2 < d({\bf s}_{\dagger}, {\bf s}_{\sharp})^2,}
e.g., ${\bf s}_1$ or ${\bf s}_2$ but not ${\bf s}_3$ in the figure, thus which a locally an obtuse angle and not an acute angle.

These distance and orientation complementary constraints are useful to verify the coherence of the proposed solution or to reduce the amount of search along the geodesics.

\subsubsection*{Implementation: Generating other values for exploration.} \label{generation}

A step further, we also can have the need to generate a new value, either given a current value ${\bf s}_0$ or without considering current value.

\paragraph{Using the scalar-field data as dictionnary.}~ 

Given a scalar field with predefined values, the 1st idea is to consider these as a ``dictionary'' of possible values, and paths from these values to the current value ${\bf s}_0$ and select on these paths:
\\- [closest] The closest data structure different from ${\bf s}_0$.
\\- [remote] The data structure at the highest distance from ${\bf s}_0$.
\\- [random] A random data structure different, from ${\bf s}_0$.
or a
\\- [user defined reward] Selecting a value that approximately maximizes a user defined or predefined reward, as it is the case for the trajectory generation.
\\- [user defined selection mechanism] Such a mechanism can be for instance a random selection, given a user defined distribution.
\\ We could also have considered the remote value at the highest distance below a given bound $d_{\max}$ or the closest value above a given bound $d_{\min}$ or random value in a given $[d_{\min}, d_{\max}]$ interval. This seems to be a reasonable set of interesting alternatives that can be designed, given only distances and geodesic.

A step further, if we have a default value, in the sense that it is the value as closed as possible to an undefined value, we can select:
\\- [smaller] The closest data structure different from ${\bf s}_0$, with a lower distance to the the default value than ${\bf s}_0$.
\\- [larger] The closest data structure different from ${\bf s}_0$, with a higher distance to the the default value than ${\bf s}_0$.
\\- [smallest] The data structure at the highest distance from ${\bf s}_0$, with a lower distance to the the default value than ${\bf s}_0$.
\\- [largest] The data structure at the highest distance from ${\bf s}_0$, with a higher distance to the the default value than ${\bf s}_0$.

Depending on the choosen criterion, this is in the worst case performed at a linear cost with respect to the scalar field value data size and related path lengths.

\paragraph{About hierarchical generation of a new value from a reference one.}~
 
Another track is to see to what extent we can generate a new value given a current value ${\bf s}_0$, without any dictionary. Let us first consider two simple types: enumeration, as implemented here\footnote{\url{https://line.gitlabpages.inria.fr/aide-group/symboling/EnumType.html}} and numeric types, as implemented here\footnote{\url{https://line.gitlabpages.inria.fr/aide-group/symboling/NumericType.html}}.

{\em Generating a new value among an enumeration.} In that case, we are in the same situation as when considering the scalar field values, except that we do not consider the values on the path but only the values themselves.

{\em Generating a new value for a bounded numeric value.} In the simple case of a numeric value, the previous alternatives of closest, remote, uniform random, or extrapolation are obvious to implement, and will not be detailed, please refer to the numeric data type\footnote{\url{https://line.gitlabpages.inria.fr/aide-group/symboling/numerictype.pdf}}. Here the closest value, is the value at a distance of the precision $\epsilon$, and depending on the criterion we may have to randomly choose the smaller or larger value, which is also the case if both bounds are at equal distance and a remote value is chosen.

Beyond these two scalar cases, let us now discuss to what extent we can define a new value considering the three constructions of set, list and record.

{\em Generating a closest value, given a compound of value.} For a set, list or record, we generate a new value by modifying, inserting or deleting a new element of the set and generating a {\em closest} value requires to perform once on of this operation. Thus either an insertion, of the default value of the element type, because this is the closest value to an undefined (i.e., here nonexistent) value, or modify one of the element value, modifying the element considering the closest new element value. This includes deletion, which is nothing but replacing it by an undefined value, if it is the closest value. Doing more than one modification can not minimize the distance to the new value, because it is additive in this case. The complexity of such operation is linear with the set side, and implementation is tractable.

However, defining the {\em remote} new value (i.e. a new value at a maximal distance) is meaningless here, because we always can insert an unbounded number of new values increasing the distance at will. Then, defining a remote value below a given threshold, or a closest value above a given threshold, is much more complex, because we have to consider the combination of modification of several, when not all, elements and then test all combinations, so that up to our best understanding, this is not tractable. If we do not consider element insertion of deletion, but only modification, the situation is still complex, because choosing for each element a value at maximal distance does not mean that the resulting set will be at maximal distance, because new lower distances between elements could be found. We thus will not implement remote new value for a set.

{\em Generating a random value, given a compound of value.}  It is more simple to define a {\em random} new value, defining the probability to delete an element, insert a random element or insert or modified a random field value in a record. This however highly depend upon the probability distribution, which is to be specified by the user.

{\em Generating a new value, given a record.} For a record, in our context, we consider that the list of named item is fixed, i.e., do not consider inserting a new unexpected item, while deleting an item, simply means replacing the value by a default value. Given this restriction, the distance being additive on each item, we easily can define the closest value by modifying only one item which closest value distance is minimum, a remote value by considering each item remote value, if defined (i.e. if it is an enumeration, a numerical value or a record), a random value by randomly modifying each item value, if above a random threshold.

\subsubsection*{Implementation: The open-source code}

\begin{tabular}{cc}
\includegraphics[width=0.2\textwidth]{./under-construction.png} &
\parbox{0.4\textwidth}{\vspace{-5cm} These ideas are (NOT YET ! But … on the way to be) implemented in a experimental preliminary effective object \href{./ScalarField.html}{ScalarField}.}
\end{tabular}

\newpage {\scriptsize \bibliographystyle{apalike}\bibliography{AIDE.bib,}
\end{document}
