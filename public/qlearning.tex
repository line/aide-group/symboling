%
% This file is used in  
%   https://gitlab.inria.fr/line/aide-group/aide/-/blob/master/etc/tex_to_pdf.sh
% and command are documented in http://aide-line.inria.fr/build/www/etc.html#.tex_to_pdf
%

% Defines a minimal layout for pdf display

\documentclass[a4paper,12pt,landscape,pdftoolbar=false,pdfmenubar=false]{article}
\pagestyle{empty} 
\topmargin 0cm \oddsidemargin 0cm \evensidemargin 0cm 
\setlength{\parindent}{0in} 
\setlength{\parskip}{3mm} 
\usepackage[margin=2cm]{geometry}

% Here are the used packages

\usepackage[utf8]{inputenc}
\DeclareUnicodeCharacter{00B0}{\textsuperscript{o}}

\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{amsmath}\usepackage{amssymb}\usepackage{amsfonts}
\usepackage{array}
\usepackage{times}
\usepackage{color}
%\usepackage{algpseudocode}\usepackage{algorithm}
\usepackage{listings}

% Here is tha macro for conditional compilation

\def\aidebuild {}

% Here are some used commands

\newcommand{\deq}{\stackrel {\rm def}{=}} 
\newcommand{\eqline}[1]{~\vspace{0.3cm}\\\centerline{$#1$}\vspace{0.3cm}\\}
\newcommand{\tab}{\hphantom{6mm}}
\newcommand{\hhref}[1]{\href{#1}{#1}}

% Here are some homemade commands to define minimal slides

\newcommand{\slide}[1]{\clearpage\fbox{\parbox[t][16.5cm][t]{\textwidth}{\Huge #1}}\newpage}
\newcommand{\stitle}[1]{~\vspace{1cm}\\\centerline{\fontsize{40}{50}\selectfont \bf #1}\vspace{0.5cm}\\}
\newcommand{\sright}[1]{\begin{flushright}#1\tab\end{flushright}}
\newcommand{\scenter}[1]{\begin{center}#1\end{center}}
\newcommand{\stwo}[4]{\begin{tabular}{ll}\parbox{#1\textwidth}{#3}&\parbox{#2\textwidth}{#4}\end{tabular}}

\begin{document}

\vspace{4cm} \centerline{\Huge Q-learning in a symboling space}

\subsubsection*{Position of the problem: Reinforcement learning in a non enumerable state space}

We consider a problem-solving task at a geometric level, considering being located somewhere in a \href{./symboling.pdf}{symbolic state-space}, with the goal of having an autonomous behavior in interaction with an environment. We consider a reinforcement learning mechanism, in one of its simpler instance: Q-Learning\footnote{\url{https://en.wikipedia.org/wiki/Q-learning}}. The key point here is that we do not consider an enumerable finite Markov decision process, bit a complex high-dimensional symbolic state space as introduced in\footnote{\url{https://hal.inria.fr/hal-03327706/document}} \cite{mercier_reinforcement_2021}, see also the \href{https://hal.inria.fr/hal-03327706/file/ICANN21_poster.pdf}{\underline{poster}} and a \href{https://files.inria.fr/mecsci/aide/icann'2021.mp4}{\underline{video presentation}}.

\subsubsection*{Background: Optimal Q-learning mechanism}

We consider an agent, at a given discrete time $t$, which is in a state $s_t \in {\cal A}$ within a set of possible states and perform an action $a_t \in {\cal A}$ within a set of possible actions, and then observe its state new state $s_{t+1}$ while receiving a reward $r_{t+1} \in {\cal R}$ consequence of its action $a_t$. Its goal is to maximize the average reward:
\eqline{R=\sum_{t=0}^{\infty }\gamma^{t} \, r_{t}}
where $\gamma$ is a discount factor. Here the situation is considered as stationary and memory-less, in the sense that the reward is only a function of the present state and action value. On simple way to obtain an algorithm converging to an optimal behavior is to get track of $Q(s, a)$ which is the average reward given a state and action value, and then choose the action that will maximize this reward given the observed state, i.e., $a_t \deq \mbox{arg max}_a Q(s_t, a)$ and the following numerical scheme\footnote{Borrowed from \url{https://en.wikipedia.org/wiki/Q-learning\#Reinforcement_learning}.}:
\eqline{\displaystyle Q^{new}(s_{t},a_{t})\leftarrow \underbrace {Q(s_{t},a_{t})} _{\text{current value}}+\underbrace {\alpha } _{\text{learning rate}}\cdot \overbrace {{\bigg (}\underbrace {\underbrace {r_{t+1}} _{\text{reward}}+\underbrace {\gamma } _{\text{discount factor}}\cdot \underbrace {\max _{a}Q(s_{t+1},a)} _{\text{estimate of optimal future value}}} _{\text{new value (temporal difference target)}}-\underbrace {Q(s_{t},a_{t})} _{\text{current value}}{\bigg )}} ^{\text{temporal difference}}}
converges towards an optimal value by adding the maximum reward attainable from future states to the reward for achieving its current state, effectively influencing the current action by the potential future reward.

There are two meta-parameters: The discount factor $\gamma \in [0, 1[$ determines the importance of future rewards, and is application dependent, the higher the larger the ``temporal window'', the lower the value the higher the algorithmic stability. The learning rate $\alpha \in [0, 1]$ determines to what extent newly acquired information overrides old information, with high value in a deterministic case and lower values if the situation is stochastic. We may consider $\gamma = 1/2$ and $\alpha = 0.1$ which are standard vanilla values.

\subsubsection*{Generalization: Adaptation to a symboling state space using interpolation}

When the problem is not enumerable $Q(s, a)$ function must be approximated and Deep Reinforcement Learning methods (see e.g., \cite{wang_learning_2017} for a review) use deep neural networks to either approximate the $Q(s, a)$ function or other model-free or model-based algorithmic mechanisms. The major limitation of such method is the need of a massive amounts of training data, and the quoted authors propose a hierarchical solution involving meta-learning monitoring of the learning mechanisms. In direct link with our symbolic approach \cite{garcez_towards_2018} have considered a rather simple state space encoding symbolic information, in link with common sense reasoning, and have obtained very good performances in term of transfer learning. The key idea is to propose an architecture where a numerical deep reinforcement learning algorithm interacts with a symbolic module. Using a similar cooperation between numerical and symbolic modules, also introducing a hierarchical architecture \cite{ma_interpretable_2020} have proposed a solution with the additional goal to consider interpretable learning at the symbolic level. These is a very challenging and still open subject as discussed informally in \cite{harris_deep_2020}.

Here the approach, following \cite{mercier_reinforcement_2021}, is less ambitious and more technical: To what extents could classical reinforcement learning algorithms not interact but intrinsically manipulates symbolic information? Considering Q-learning with state and action being symbolic data structures how can we adapt the former algorithmic scheme?

We are in the case of symbolic data structures with a metrizable embedding, i.e., a ``symboling''. In such a symbolic space we are only equipped with :
\\\tab 1/ An editing distance between two values, providing also corresponding path from none value to another,
\\\tab 2/ A projection operator from a value onto a region of a spate space (a region is called a type in this context).

One key point is that, given the largeness of the state space, each state value is very likely different from another, so that one state value is very likely visited once, making it impossible to use the usual update rule on tabulated values. In order to overcome this barrier, we simply propose to interpolate, i.e., keeping trace of all preceding values, to approximate
$Q(s, a)$ considering known values via a formula of, e.g., the form:
\eqline{Q(s, a) = \sum_{s_t, a_t} e^{-(d(s, s_t) + d(a, a_t))/\rho} \, Q[s_t, a_t] \left/ \sum_{s_t, a_t} e^{-(d(s, s_t) + d(a+, a_t))/\rho} \right.}
Here we choose an exponential decreasing weighting (but this is to be checked numerically, other solutions could be better), thus introducing another meta-parameter $\rho > 0$ which allows to scale the neighborhood size to consider. Such calculation is linear with respect to the $Q-learning$ table size $S$ and require to compute $(O(S^2)$ distances.

A step further, we propose to update the $Q-learning$ table again by interpolation, i.e., considering a weighted formula of the form:

\centerline{\includegraphics[width=0.6\textwidth]{./qlearning-algorithm-structure.png}}

The second barrier is that the possible actions are not necessarily enumerable so that we cannot explicitly compute the action corresponding to the maximal expected value. There is however two bypassing:
\\- Do not restrain the state space but still restrain the possible action to set of enumerable values.
\\- Consider existing actions and compute some paths between such actions to generate new intermediate actions.
\\- Explore new actions, considering a new mechanism allowing to
\\\tab 3/ Generate a value around a given current value
\\in order to have the complete set of tools to generate a Q-learner in such ``symboling'' space. This is discussed in the \href{./trajectory.pdf}{trajectory} documentation.

\subsubsection*{Implementation: ... coming soon :)}

\begin{tabular}{cc}
\includegraphics[width=0.2\textwidth]{./under-construction.png} &
\parbox{0.4\textwidth}{\vspace{-5cm} These ideas are (NOT YET ! But … on the way to be) implemented in a experimental preliminary effective object \href{./QLearning.html}{QLearning}.}
\end{tabular}





\newpage {\scriptsize \bibliographystyle{apalike}\bibliography{AIDE.bib,}
\end{document}
