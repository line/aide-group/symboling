# Verification of superharmonic potential derivations

##### Ref: https://en.wikipedia.org/wiki/Subharmonic_function#Riesz_Representation_Theorem

with(LinearAlgebra):
with(VectorCalculus):

dim := 4:

## Derivation for d > 2

x := Vector(dim, i -> cat('x_', i)):

V1 := 1 / Norm(x, 2)^d:

g1 := Vector(dim, i -> -d * x[i] / Norm(x, 2)^(d+2)):

ok11 := evalb(0 = Norm(simplify(g1 - convert(Gradient(V1, convert(x, list)), Vector)), 2));

L1 := ((d+2) - dim) * d / Norm(x, 2)^(d+2):

ok12 := evalb(0 = simplify(L1 - Laplacian(V1, convert(x, list))));

r_d := simplify(solve({1/r^d = 1/2},{r}));

## Derivation for d = 2, with unbounded influence

V2 := -log(Norm(x, 2)):

g2 := Vector(dim, i -> -x[i] / Norm(x, 2)^2):

ok21 := evalb(0 = Norm(simplify(g2 - convert(Gradient(V2, convert(x, list)), Vector)), 2));

L2 := (2 - dim) / Norm(x, 2)^2:

ok22 := evalb(0 = simplify(L2 - Laplacian(V2, convert(x, list))));

r_d := simplify(solve({-log(r) = 1/2},{r}));

## Graphic representation of the influence radius

#plot([-log(r),1/r,1/r^2,1/r^3,1/r^4,1/r^5],r=0..4);

## Derivation for d > 2, with a local metric

Lambda := Matrix(dim, dim, (i, j) -> if i > j then cat('lambda_', i, j) else cat('lambda_', j, i) fi):

norm2 := (x, Lambda) -> Multiply(Transpose(x), Multiply(Lambda, x))^(1/2):

V3 := 1 / norm2(x, Lambda)^d:

g3 := simplify(-d / norm2(x, Lambda)^(d+2) * Multiply(Lambda, x)):

ok31 := evalb(0 = Norm(simplify(g3 - convert(Gradient(V3, convert(x, list)), Vector)), 2));

L3 := d / norm2(x, Lambda)^(d+2) *
      ((d + 2) * (norm2(x, Multiply(Lambda, Lambda)) / norm2(x, Lambda))^2 - Trace(Lambda)):

ok30 := evalb(0 = simplify(L1 - subs({seq(seq('cat('lambda_', i, j) = if i = j then 1 else 0 fi', i = 1..dim), j = 1..dim)}, L3)));

ok32 := evalb(0 = simplify(L3 - Laplacian(V3, convert(x, list))));

## Derivation of the Laplace-Beltrami for G locally constant

G := Matrix(dim, dim, (i, j) -> cat('g_', i, j)):

L4 := d * ((d + 2) * (norm2(x, G) / Norm(x, 2))^2 -
           Trace(G)) / Norm(x, 2)^(d+2):

L5 := d * ((d + 2) * (norm2(x, Multiply(Lambda, Multiply(G, Lambda))) / norm2(x, Lambda))^2 -
           Trace(Multiply(G, Lambda))) / norm2(x, Lambda)^(d+2):

#### g = sqrt(|G|)

L4_ := add(add(1/g * diff(g * G[i, j] * diff(V1, x[i]), x[j]), i = 1 .. dim), j = 1 .. dim):

L5_ := add(add(1/g * diff(g * G[i, j] * diff(V3, x[i]), x[j]), i = 1 .. dim), j = 1 .. dim):

ok4 := evalb(0 = simplify(L4 - L4_));

ok5 := evalb(0 = simplify(L5 - L5_));

## Derivation of a general distance

V0 := 1 / delta(x)^d:

#### \nabla V(x) = -\nabla delta(x) / delta(x)^(d+1):

g0 := -d * Gradient(delta(x), convert(x, list)) / delta(x)^(d+1):

g0_ := Gradient(V0, convert(x, list)):

ok01 := evalb(0 = Norm(convert(g0 - g0_, Vector), 2));

#### \Delta V(x) = d ((d+1) \|\nabla delta(x)\|^2 / delta(x) - \Delta delta(x)) / delta(x)^(d+1)

L0 := d / delta(x)^(d+1) * ((d+1) * Norm(convert(Gradient(delta(x), convert(x, list)), Vector), 2)^2 / delta(x)
      - Laplacian(delta(x), convert(x, list))):

L0_ := simplify(Laplacian(V0, convert(x, list))):

ok02 := evalb(0 = simplify(L0 - L0_));


