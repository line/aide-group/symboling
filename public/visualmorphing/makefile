define USAGE
 Usage: make [build|rebuild] i1=file1.svg i2=file2.svg [introduction=introduction.html]
  Generates a file1-file2.html file showing a sequence of intermediate images between file1 and file2.
endef
export USAGE

ifeq (,$(i1))
usage: usage
else
ifeq (,$(i2))
usage: usage
else
ifeq (,$(introduction))
introduction = introduction.html
endif

input1 = $(basename $(i1))
input2 = $(basename $(i2))
output = $(notdir $(input1))-$(notdir $(input2))

build:
# Creates JSON files from SVG
	$(MAKE) $(input1).json $(input2).json
# Runs the trajectory algorithm
	$(MAKE) -C ../../src build_cpp
	../../node_modules/.bin/json2path svg_face $(input1).json $(input2).json
# Creates the output file and the related SVG and GIF files from the JSON path outputs
	$(MAKE) $(output).html $(output).gif

rebuild:
# Cleans target files to rerun each step
	rm -f $(input1).json $(input2).json path-$(input1)-$(input2)-*.* $(output).gif $(output).html
	$(MAKE)

# Concatenates the trajectory images as an animated GIF
$(output).gif: $(wildcard path-$(input1)-$(input2)-*.json)
	files="$(patsubst %.json,%.gif,$^)" ;\
	$(MAKE) $$files OUTPUT=TRUE ;\
	gifsicle -l -d 80 -O --colors 256 -o $(output).gif $$files ;\
	rm -f $$files

# Creates an output page showing the results
$(output).html: $(wildcard path-$(input1)-$(input2)-*.json)
	files="$(patsubst %.json,%.svg,$^)" ;\
	$(MAKE) $$files OUTPUT=true ;\
	(echo "<html><!-- Generated by makefile do not edit --><body><b>Visualization of a visual morphing: $(input1) => $(input2)</b><div>This corresponds to a geodesic path considering a <a target='_blank' href='../SvgType.html'>SVG</a> symboling object.</div>" ;\
         if [ -f '$(introduction)' ] ; then cat '$(introduction)' ; fi ;\
	 echo "<center><img src='./$(output).gif'/><hr/></center><hr/><i>Image sequence:</i><br/><table border='1'>" ;\
	 for file in $$files ; do echo "<tr><td width='500' height='500'>"; cat $$file ; echo "</td></tr>" ; done ;\
	 echo "</table></body></html>") > $@

endif
endif

#
# Conversion utilitaries
#

ifeq (,$(OUTPUT))

%.json: %.svg
	../../src/svg2json.js < $^ > $@

else

%.svg: %.json
	../../src/json2svg.js < $^ > $@

%.gif: %.svg
	convert $^ $@

endif

#
# Testing the SVG -> JSON -> SVG coherence by visualization
#

ifndef input1
input1 = toto
endif

test:
	$(MAKE) $(input1).json
	cp $(input1).json test.json
	$(MAKE) test.svg OUTPUT=true
	convert $(input1).svg test-1.gif
	convert test.svg test-2.gif
	gifsicle -l -d 80 -O --colors 256 -o test.gif test-[12].gif
	eom test.gif
	rm -f $(input1).json test*.*

usage:
	echo "$$USAGE"

