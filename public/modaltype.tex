%
% This file is used in  
%   https://gitlab.inria.fr/line/aide-group/aide/-/blob/master/etc/tex_to_pdf.sh
% and command are documented in http://aide-line.inria.fr/build/www/etc.html#.tex_to_pdf
%

% Defines a minimal layout for pdf display

\documentclass[a4paper,12pt,landscape,pdftoolbar=false,pdfmenubar=false]{article}
\pagestyle{empty} 
\topmargin 0cm \oddsidemargin 0cm \evensidemargin 0cm 
\setlength{\parindent}{0in} 
\setlength{\parskip}{3mm} 
\usepackage[margin=2cm]{geometry}

% Here are the used packages

\usepackage[utf8]{inputenc}
\DeclareUnicodeCharacter{00B0}{\textsuperscript{o}}

\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{amsmath}\usepackage{amssymb}\usepackage{amsfonts}
\usepackage{array}
\usepackage{times}
\usepackage{color}
%\usepackage{algpseudocode}\usepackage{algorithm}
\usepackage{listings}

% Here is tha macro for conditional compilation

\def\aidebuild {}

% Here are some used commands

\newcommand{\deq}{\stackrel {\rm def}{=}} 
\newcommand{\eqline}[1]{~\vspace{0.3cm}\\\centerline{$#1$}\vspace{0.3cm}\\}
\newcommand{\tab}{\hphantom{6mm}}
\newcommand{\hhref}[1]{\href{#1}{#1}}

% Here are some homemade commands to define minimal slides

\newcommand{\slide}[1]{\clearpage\fbox{\parbox[t][16.5cm][t]{\textwidth}{\Huge #1}}\newpage}
\newcommand{\stitle}[1]{~\vspace{1cm}\\\centerline{\fontsize{40}{50}\selectfont \bf #1}\vspace{0.5cm}\\}
\newcommand{\sright}[1]{\begin{flushright}#1\tab\end{flushright}}
\newcommand{\scenter}[1]{\begin{center}#1\end{center}}
\newcommand{\stwo}[4]{\begin{tabular}{ll}\parbox{#1\textwidth}{#3}&\parbox{#2\textwidth}{#4}\end{tabular}}

\begin{document}
\subsubsection*{Modal type description}

The Boolean notion of being either false or true is generalized as a numeric representation of partial knowledge, considering a value $\tau \in [-1, 1]$:

\centerline{\includegraphics[width=0.85\textwidth]{./possibility-necessity.png}}

The true value corresponds to 1 (fully possible and fully necessary), the false value to -1 (neither possible nor necessary, i.e., impossible) and the unknown value to 0, which corresponds to a fully possible but absolutely not necessary value\footnote{This representation has been designed to be compatible with the ternary Kleene logic, and is in one to one correspondence with respect to the possibility theory. Possibility theory is devoted to the modeling of incomplete information, in link with an observer belief regarding a potential event and surprise after the event occurrence (please refer to \cite{denoeux_representations_2020} for a general introduction). While almost all partially known information is related to probability, human “level of truth” is more subtle and related to possibility and necessity, as formalized in the possibility theory, as discussed in \cite{denoeux_representations_2020} and \cite{denoeux_representations_2020-1}, in link with modal logic, i.e., something true in a “given context” \cite{fischer_modal_2018}, which is also considered as representative to what is modeled in educational science and philosophy \cite{rusawuk_possibility_2018}, because it corresponds to commonsense reasoning in the Piaget's sense \cite{smith_development_1994}, taking exceptions into account, i.e., considering non-monotonic reasoning. Furthermore, in symbolic artificial intelligence, i.e., knowledge representation through ontology, the link has been built between this necessity/possibility dual representation and ontology \cite{tettamanzi_possibilistic_2017}. This must be understood as a deterministic theory, in the sense that partial knowledge is not represented by randomness. See \cite{vallaeys_generaliser_2021,vieville_representation_2022} for an extension taking randomness into account.}. In between, negative values correspond to partially possible event and positive value to partially necessary event.

Boolean true/false values and Kleene three value logic true/unknown/false values conjunction ({\tt and}), disjunction ({\tt or}), exclusive disjunction ({\tt xor}) and element set difference generalisations correspond to $\min$, $\max$, and polynomial operators.

\newpage\begin{tabular}{ll}
\includegraphics[width=0.4\textwidth]{./possibility-projection.png} &
\parbox[t]{0.6\textwidth}{\vspace{-8.5cm}
Our representation $\tau \in [-1,1]$ is in correspondance with modal logic, given possibility $\pi$, and necessity $\nu$, with $0 \le \nu \le \pi \le 1$ by construction:
\eqline{
\left\{\begin{array}{rclcl}
\tau &\deq& \nu + \pi - 1 &\in& [-1, 1]\\
\rho &\deq& \nu - \pi + 1 &\in& [0, 1]\\
\end{array}\right.
\mbox{ with } \left\{\begin{array}{rclcr}
\pi &=& \frac{\tau - \rho}{2} + 1 &=& \left. 1 + H(-\tau) \, \tau \right|_{\nu \, (\pi - 1) = 0} \\
\nu &=& \frac{\tau + \rho}{2} &=& \left. \;\;\; H(\tau) \, \tau  \right|_{\nu \, (\pi - 1) = 0} , 
\end{array}\right.}
In the standard deterministic case \cite{denoeux_representations_2020} the correspondance is a one to one, because:
\eqline{\nu > 0 \Rightarrow \pi = 1 \mbox{ and } \pi < 1 \Rightarrow \nu = 0 \mbox{, thus } \nu \, (\pi - 1) = 0}
this is a 1D representation with $\rho = \pm \tau$, where either $\pi$ or $\nu$ varies, corresponding to the thick segment line of the figure. \\ In the generalized case \cite{vallaeys_generaliser_2021,vieville_representation_2022} $\nu$ and $\pi$ bound a probability $p$, i.e., $\nu \le p \le \pi$. If $\nu = \pi$ we are in a pure random case with a known probablity estimation $p = (\tau+1)/2$. \\ We also can define the projection of any value $(\pi, \nu)$, using:
\eqline{\tau \leftarrow \max(-1, \min(1, \nu + \pi - 1)), \rho \leftarrow \max(|\tau|, \min(1, \nu - \pi + 1))}
which partition the $(\pi, \nu)$ space in four oblique regions of false, partially possible, partially necessary and true regions. 

}\\ \end{tabular}

\newpage {\scriptsize \bibliographystyle{apalike}\bibliography{AIDE.bib,}
\end{document}
