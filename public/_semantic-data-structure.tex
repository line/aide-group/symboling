
At the computing level, we aim at manipulating the symbolic representation of knowledge of the form shown in Fig.~\ref{concept-grounding}, as introduced in \cite{mercier_where_2023}. In our context, we represent concepts as a hierarchical data structure. Concepts are anchored in an  input/output, i.e., stimulus/response, framework, which might consist of sensorimotor feature spaces (colored regions) corresponding, for example, to different sensor modalities. Inherited features (e.g., the penguin “is-a” bird and thus inherits the features of a bird) are shown with dotted lines, while red lines represent overwritten values (e.g., a penguin can also swim but cannot fly). Green arrows point toward concepts that are themselves attributes of other concept features, accounting for inter-concept relationships.
Values are completed by meta-information that is not explicitly manipulated by the agent but is used for process specification or interpretation (e.g., the weight unit and bounds). From \cite{mercier_where_2023}.

\begin{figure}[htbp]
\centerline{\includegraphics[width=\textwidth,height=0.5\textheight]{concept-grounding.png}}
\caption{Hierarchical data structure representing concpets, see text.}\label{concept-grounding}
\end{figure}

At the modeling level, we follow \cite{gardenfors_conceptual_2004}, with the simple idea that an individual resource can be defined by ``feature dimensions,'' i.e., attributes with some typed value. For instance, a bird could be the following\footnote{The syntax used is a weak form of the \href{JSON}{https://www.json.org} syntax: \url{https://line.gitlabpages.inria.fr/aide-group/wjson}.}:

\begin{lstlisting}[basicstyle=\small]
bird: {
  is_a: vertebrate
  can: { sing fly eat: { worm fish } }
  has: { feather beak }
  is: { weight : { min: 0.010 max: 50 unit: kilogram } }
},\end{lstlisting}
with some exceptions like penguins:
\begin{lstlisting}[basicstyle=\small]
penguin: {
  is_a: bird
  can: { fly: false walk }
}.
\end{lstlisting}

Here, we choose the general approach of semantic knowledge representation using a hierarchical taxonomy ({\tt is-a}) with capability features ({\tt can}), including those related to other resources, extrinsic features ({\tt has}), and intrinsic features ({\tt is}) \cite{mcclelland_parallel_2003}.
This illustrative example is sufficient to allow us to detail the main characteristics of our representation.
Some features are properties, and others are relations. A property can be qualitative, e.g., the {\tt is-covered-by} property takes a value in an enumeration (e.g., \{{\tt sing}, {\tt fly}\}), or quantitative (e.g., the {\tt weight}). The features can be hierarchical, either because the value is an enumeration (e.g., {\tt can}) or because the value has some features (e.g., {\tt weight}).

Such a data structure defines a ``concept'' in the sense of \cite{gardenfors_conceptual_2004} (e.g., ``a bird''), which is both a convex region of the state space (e.g., the region of all birds) and a prototype: Each feature has a default value, and this also defines a prototype (e.g., a typical, i.e., prototypical, bird). It corresponds to the third cognitive memory architecture, as proposed by \cite{eichenbaum_memory_2017}. At the programming level it is going to be implemented as a ``type''. At the geometric level, data value correspond to points and concept to regions, but with tricky property: Any data structure is the prototype of a region, as detailled in the next section.

When defining such data structure, there are obviously several design choices and the following general recommendation might be useful:
\\- {\em Atomic value}: It is always better to decompose the information as much as possible in atomic irreducible elements (e.g., <tt>{family\_name: Smith first\_names: [John Adam]}</tt> instead of <tt>{name: 'Smith, John Adam' }</tt>) for algorithmic processing.
\\- {\em Maximal tree structure}: It is always better to organize features in sub-structure than to present flatten information (e.g., create a sub-structure for the name, birth-date, etc…) in order to maximize modularity.
\\ while we already mentioned the importance of providing as much as possible default value,
\\ while it is always preferable to choose explicit and standard names for features, i.e., look at already established vocabulary, otherwise avoid acronym or abbreviation, but chose the most common word for the feature to name.

Using Vector Symbolic Architecture implemented at the neural spiking assembly level thanks to the Neural Engineering Framework \cite{eliasmith_how_2013}, such a cognitive symbolic data structure can be implemented as biologically plausible memory, allowing to manipulate it conjointly at both a symbolic and numeric level \cite{mercier_where_2023}.
