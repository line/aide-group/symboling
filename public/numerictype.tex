%
% This file is used in  
%   https://gitlab.inria.fr/line/aide-group/aide/-/blob/master/etc/tex_to_pdf.sh
% and command are documented in http://aide-line.inria.fr/build/www/etc.html#.tex_to_pdf
%

% Defines a minimal layout for pdf display

\documentclass[a4paper,12pt,landscape,pdftoolbar=false,pdfmenubar=false]{article}
\pagestyle{empty} 
\topmargin 0cm \oddsidemargin 0cm \evensidemargin 0cm 
\setlength{\parindent}{0in} 
\setlength{\parskip}{3mm} 
\usepackage[margin=2cm]{geometry}

% Here are the used packages

\usepackage[utf8]{inputenc}
\DeclareUnicodeCharacter{00B0}{\textsuperscript{o}}

\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{amsmath}\usepackage{amssymb}\usepackage{amsfonts}
\usepackage{array}
\usepackage{times}
\usepackage{color}
%\usepackage{algpseudocode}\usepackage{algorithm}
\usepackage{listings}

% Here is tha macro for conditional compilation

\def\aidebuild {}

% Here are some used commands

\newcommand{\deq}{\stackrel {\rm def}{=}} 
\newcommand{\eqline}[1]{~\vspace{0.3cm}\\\centerline{$#1$}\vspace{0.3cm}\\}
\newcommand{\tab}{\hphantom{6mm}}
\newcommand{\hhref}[1]{\href{#1}{#1}}

% Here are some homemade commands to define minimal slides

\newcommand{\slide}[1]{\clearpage\fbox{\parbox[t][16.5cm][t]{\textwidth}{\Huge #1}}\newpage}
\newcommand{\stitle}[1]{~\vspace{1cm}\\\centerline{\fontsize{40}{50}\selectfont \bf #1}\vspace{0.5cm}\\}
\newcommand{\sright}[1]{\begin{flushright}#1\tab\end{flushright}}
\newcommand{\scenter}[1]{\begin{center}#1\end{center}}
\newcommand{\stwo}[4]{\begin{tabular}{ll}\parbox{#1\textwidth}{#3}&\parbox{#2\textwidth}{#4}\end{tabular}}

\begin{document}
\subsubsection*{Numeric type description}

Numerical quantity related to a sensory input or an algorithm numerical value, corresponds to a bounded value (between minimal and maximal bounds), up to a given precision threshold (above which two values may differs and below which two values are indistinguishable, being equal or not), an approximate neighborhood sampling size or ``step'' (below which two distinct values are in the same local area), a default value (used in initialization, or to avoid undefined value), expressed in a given unit (e.g., second, meter, etc), if any. 

\centerline{\includegraphics[width=0.85\textwidth]{./measure-parameterization.png}}

Such specification is important to properly manipulate quantitative information. In particular, the values can be normalized (e.g., mapped onto the $[-1, 1]$ interval) and mapped onto a finite set of relevant values. One consequence is that algorithm precision thresholds can be deduced (often using first order approximations), spurious values can be detected, numerical conditioning of algorithms is enhanced, and so on (see \cite{vieville_implementing_2001} for a discussion). At the computational specification level, these parameters define a sub-type of usual numerical types, yielding a better definition of the related code.

This concerns numerical sensory data and internal quantitative data (e.g, derived data, calculation output, etc). A step further, symbolically coded data can always be sampled (e.g., a vector font drawn on a canvas and then sampled as pixels) at a given precision.

It is obvious that any quantitative measure is bounded (e.g. physical velocity magnitude stands between 0, for a still object, and the light speed) and is given up to a given precision (e.g., a localization in an image is given up to one pixel size, a school ruler up to 1 millimeter graduation). The key point is that it is useful to make explicit this obvious fact (e.g., that any measurement device has a given precision and a measurement range) at the specification level instead of using it implicitly when required.

The notion of positive sampling step, in order to define a local neighborhood size, is used to weight distance calculation, and to properly sample the data space: The underlying idea is that the state space is locally convex so that in a given neighborhood local search of an optimum yields to the optimal local value. This idea have been implemented in the \href{https://line.gitlabpages.inria.fr/aide-group/stepsolver}{stepsolver} variational solver, i.e., optimizer and controller.

Given bounds $[min..max]$ and a default value $zero$, with $min < zero < max$, the transform\footnote{Here $signum(x) \deq x/|x|$ with $signum(0) \deq 0$.} $u_\infty(x) \deq \lim_{n \rightarrow +\infty} u_n(x)$
\eqline{\begin{array}{rclrclrcl}
 u_0 &\leftarrow& \frac{2 \, x - (min + max)}{max - min}, &
     z_0 &\deq& \left. u_0 \right|_{x = zero}, \\
 u_{n+1} &\leftarrow& u_n - \frac{\tilde{z}_n}{1 - \tilde{z}_n^2} \, (1 - u_n^2), &
     \tilde{z}_n &\deq& \min(z_{n-1}, \mbox{signum}(z_0) \, (\sqrt{2} - 1)), &z_{n+1} &\deq& \left. u_{n+1} \right|_{u_n = z_n}, \\
 \end{array}}
is monotonic and convergent with $z_{n+1} = 0$ after a finite number of iteration as soon as $|z_n| < (\sqrt{2} - 1)$, allows to map $u_\infty(min) = -1$, $u_\infty(max) = 1$, $u_\infty(zero) = 0$, corresponds to a simple linear scaling if $zero = (min + max)/2$ and a quadratic scaling if $|z_0| \leq |\sqrt{2} - 1|$, up to a polynomial of degree 16 (thus height iterations) when at about 1\% of a bound, as verified by obvious symbolic derivation\footnote{See \url{https://line.gitlabpages.inria.fr/aide-group/symboling/numeric-normalize.html}.}.

This specification induces a \href{https://en.wikipedia.org/wiki/Pseudometric_space}{pseudometric}:
\eqline{d(x, x') = \frac{|x - x'|}{step}, |x - x'| > precision \Rightarrow x \neq x',}
in words, the distance is weighted by the $step$, i.e., the neighborhood approximate size, while if two differs by a quantity below than the precision threshold, they are indistinguishable (thus either equal or not), so that we can decide if two values are different but not decide about their equality.

\newpage {\scriptsize \bibliographystyle{apalike}\bibliography{AIDE.bib,}
\end{document}
